#!/usr/bin/env bash

pyuic5 fMainUI.ui -o fMainUI.py
pyuic5 dlgAddEmailboxUI.ui -o dlgAddEmailboxUI.py
pyuic5 dlgEditEmailboxUI.ui -o dlgEditEmailboxUI.py
pyuic5 dlgAboutUI.ui -o dlgAboutUI.py
pyuic5 dlgPopupMsgUI.ui -o dlgPopupMsgUI.py
