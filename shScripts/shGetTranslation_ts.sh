#!/usr/bin/env bash

pylupdate5 fMain.py fMainUI.py fMainUI.ui -ts checkEmail_fMain_ru.ts
pylupdate5 dlgAddEmailbox.py dlgAddEmailboxUI.py dlgAddEmailboxUI.ui -ts checkEmail_dlgAddEmbx_ru.ts
pylupdate5 dlgEditEmailbox.py dlgEditEmailboxUI.py dlgEditEmailboxUI.ui -ts checkEmail_dlgEdEmbx_ru.ts
pylupdate5 dlgAbout.py dlgAboutUI.py dlgAboutUI.ui -ts checkEmail_dlgAbout_ru.ts
pylupdate5 dlgPopupMsg.py dlgPopupMsgUI.py dlgPopupMsgUI.ui -ts checkEmail_dlgPopupMsg_ru.ts
