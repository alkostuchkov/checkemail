#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt5.QtCore import QRunnable, QObject, pyqtSignal
from MyClasses import cAuthentification


class CLoginSignals(QObject):
    """  """
    finished = pyqtSignal()
    error = pyqtSignal(tuple)


class CLogin(QRunnable):
    """  """
    def __init__(self, login, password, serverName):
        QRunnable.__init__(self)
        self.signals = CLoginSignals()
        self._login = login
        self._password = password
        self._serverName = serverName

    def run(self):
        auth = cAuthentification.CAuthentification()
        try:
            if self._serverName == "imap.gmail.com":
                auth.authGMail(self._login, self._password)
            elif self._serverName == "imap.yandex.com":
                auth.authYandex(self._login, self._password)
            elif self._serverName == "imap.mail.ru":
                auth.authMailru(self._login, self._password)
            elif self._serverName == "imap.rambler.ru":
                pass
                # auth.authRambler(fullEmail, dCheckEmbx[fullEmail][1])
        except Exception as err:
            self.signals.error.emit((err, ))
            # exctype, value = sys.exc_info()[:2]
            # self.signals.error.emit((exctype, value, traceback.format_exc()))
            # return
        else:
#todo delete
            print("else login finished...")
            # self.signals.finished.emit()

