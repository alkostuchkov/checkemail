# -*- coding: utf-8 -*-

from PyQt5.QtCore import QRunnable, QObject, pyqtSignal
import urllib.request


class CCheckInternetSignals(QObject):
    """ Signals for CCheckInternet class """
    result = pyqtSignal(bool)


class CCheckInternet(QRunnable):
    """ Thread is run by self._tmrCheckInternet in fMain.py """

    def __init__(self):
        QRunnable.__init__(self)
        self.signals = CCheckInternetSignals()

    def run(self):
        try:
            urllib.request.urlopen("http://www.google.com")
            self.signals.result.emit(True)
        except:
            try:  # if www.google.com is not available try www.yandex.ru
                urllib.request.urlopen("http://www.yandex.ru")
                self.signals.result.emit(True)
            except:
                self.signals.result.emit(False)
        finally:
            print("DoCheckInternetConnection (QRunnable) finished...")