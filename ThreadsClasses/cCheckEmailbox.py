#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt5 import QtCore


class CCheckEmailbox(QtCore.QObject):
    """ Thread is run by actCheckEmbxs_triggered and actCheckSelectedEmbxs_triggered in fMain.py """
    emlbxsChecked = QtCore.pyqtSignal(dict)
    finished = QtCore.pyqtSignal()

    def __init__(self, myCheckEmbx, selectedEmbxsList = [], parent=None):
        QtCore.QObject.__init__(self, parent)
        self._myCheckEmbx = myCheckEmbx
        self._selectedEmbxsList = selectedEmbxsList

    @QtCore.pyqtSlot()
    def checkEmailbox(self):
        """ Check Emailboxes """
        # pass empty list (if no param) => check all Emailboxes
        resultDict = self._myCheckEmbx.checkEmails(self._selectedEmbxsList)
        self.emlbxsChecked.emit(resultDict)
        # emit finished to kill this thread
        self.finished.emit()
    # todo: delete
        print("checking in CCheckEmailbox finished!!!")
