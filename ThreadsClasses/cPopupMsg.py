# -*- coding: utf-8 -*-

from PyQt5.QtCore import QRunnable, QObject, pyqtSignal
import time


class CPopupMsgSignals(QObject):
    finished = pyqtSignal()
    cancel = pyqtSignal()
    result = pyqtSignal(int)


class CPopupMsgRunnable(QRunnable):
    def __init__(self):
        QRunnable.__init__(self)
        self.signals = CPopupMsgSignals()
        self._isInterrupted = False

        self.signals.cancel.connect(self.cancel)

    def run(self):
        for i in range(100, -1, -1):
            if self._isInterrupted:
#todo: delete
                print("break finished...")
                break
            self.signals.result.emit(i)
            time.sleep(.05)
        else:
#todo: delete
            print("else finished...")
            self.signals.finished.emit()

    def cancel(self):
        self._isInterrupted = True
#todo: delete
        print("thread _popupMsg canceled...")
