# -*- coding: utf-8 -*-


class MyIMAP4ConnectionError(Exception):
    """ My Exception for imaplib.IMAP4_SSL(serverName, port) """
    def __init__(self, message="Cannot connect to IMAP4 server."):
        self.message = message

    def __str__(self):
        return self.message


class MyIMAP4LoginError(Exception):
    """ My Exception for imaplib.login(fullEmail, password) """
    def __init__(self, message="Cannot login."):
        self.message = message

    def __str__(self):
        return self.message


class MyEmailboxLoginError(Exception):
    """ My Exception for login Emailbox. """
    pass


class MyBadBrowserOrOSError(Exception):
    """ If Windows XP or unknown browser in using. """
    pass