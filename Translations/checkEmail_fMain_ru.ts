<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>fMain</name>
    <message>
        <location filename="../Gui/fMain.py" line="92"/>
        <source>Emails haven&apos;t been checked yet.</source>
        <translation>Почта еще не была проверена.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="261"/>
        <source>Login Emailbox</source>
        <translation>Залогиниться</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="204"/>
        <source>You cannot login more than one Emailbox at the same time
Choose one.</source>
        <translation>Вы не можете залогиниться более, чем для одного ящика.
Выберите один.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="211"/>
        <source>Nothing to login.
Add any Emailbox first.</source>
        <translation>Негде логиниться.
Добавьте почтовый ящик.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="215"/>
        <source>Nothing to login.
Choose one.</source>
        <translation>Выберите почтовый ящик.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="261"/>
        <source>Unknown browser.
Program supports only
Firefox, Opera or Chrome browser.</source>
        <translation>Неизвестный браузер.
Программа поддерживает только
Firefox, Opera или Chrome браузер.</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="161"/>
        <source>Start with system</source>
        <translation>Автозапуск с системой</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="286"/>
        <source>Cannot add to autorun
{}</source>
        <translation>Не могу добавить в автозапуск</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="293"/>
        <source>Cannot delete from autorun
{}</source>
        <translation>Не могу удалить из автозапуска</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="318"/>
        <source>{} path doesn&apos;t exist.
Cannot add to autorun</source>
        <translation>{} путь не найден.
Не могу добавить в автозапуск</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="331"/>
        <source>Creating autorun file error.
Cannot add to autorun</source>
        <translation>Ошибка создания файла автозапуска.
Не могу добавить в автозапуск</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="34"/>
        <source>Check Emailbox</source>
        <translation>Проверка почты</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="565"/>
        <source>Opening database</source>
        <translation>Открытие базы данных</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="565"/>
        <source>Database access error.</source>
        <translation>Ошибка открытия базы данных.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="510"/>
        <source>Edit Emailbox</source>
        <translation>Редактирование</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="499"/>
        <source>You cannot edit more than one Emailbox at the same time
Choose one.</source>
        <translation>Вы не можете редактировать более одного ящика.
Выберите один.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="506"/>
        <source>Nothing to edit.
Add any Emailbox first.</source>
        <translation>Нечего редактировать.
Сначала добавьте почтовый ящик.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="510"/>
        <source>Nothing to edit.
Choose one.</source>
        <translation>Нечего редактировать.
Выберите хотя бы один ящик.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="592"/>
        <source>Delete Emailbox</source>
        <translation>Удаление</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="543"/>
        <source>Nothing to delete.
Add any Emailbox first.</source>
        <translation>Нечего удалять.
Сначала добавьте почтовый ящик.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="548"/>
        <source>Nothing to delete.
For deleting choose an Emailbox in the list.</source>
        <translation>Нечего удалять.
Выберите почтовый ящик.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="841"/>
        <source>Check Emails</source>
        <translation>Проверка почты</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="604"/>
        <source>Check Email task is running.
You can&apos;t delete Emailboxes
untill the task is running.
Wait untill the task will finish and try again.</source>
        <translation>Запущена проверка почты.
Вы не можете удалить почтовый ящик
пока запущена проверка.
Дождитесь окончания и попробуйте снова.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="576"/>
        <source>Are you sure you want to delete selected Emailboxes?</source>
        <translation>Вы уверены, что хотите удалить выбранные ящики?</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="584"/>
        <source>Deleting Emailbox Error.</source>
        <translation>Ошибка удаления.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="592"/>
        <source>Deleting was completed successfully.</source>
        <translation>Удаление прошло успешно.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="612"/>
        <source>Delete all Emailboxes</source>
        <translation>Удаление ВСЕХ почтовых ящиков</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="612"/>
        <source>You Database is already empty.</source>
        <translation>Ваша база данных уже пустая.</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="802"/>
        <source>Delete All Emailboxes</source>
        <translation>Удалить ВСЕ почтовые ящики</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="616"/>
        <source>This action will delete ALL Emailboxes
from Database completely.
Are you sure to do this?</source>
        <translation>Данное действие полностью удалит ВСЕ
почтовые ящики из базы данных.
Вы уверены, что этого хотите?</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="625"/>
        <source>Deleting ALL Emailboxes error.</source>
        <translation>Ошибка удаления ВСЕХ ящиков.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="633"/>
        <source>Deleting ALL Emailboxes was completed successfully.</source>
        <translation>ВСЕ почтовые ящики были успешно удалены.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="1097"/>
        <source>Checking. Please wait...</source>
        <translation>Проверка. Пожалуйста, подождите...</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="655"/>
        <source> has&apos;t been checked.</source>
        <translation> не был проверен.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="703"/>
        <source>No Internet connection.</source>
        <translation>Отсутствует интернет соединение.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="768"/>
        <source>There are no Emailboxes in the Database.
Add any Emailbox first.</source>
        <translation>База данных пуста.
Добавьте хотя бы один почтовый ящик.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="806"/>
        <source>Check Email task is running.
Wait untill the task will finish and try again.</source>
        <translation>Проверка почты уже запущена.
Дождитесь окончания.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="778"/>
        <source>There is noone selected Emailbox.
Select at least one.</source>
        <translation>Выберите хотя бы один почтовый ящик.</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="841"/>
        <source>Cannot login.
Check:
   login
   Email server
   or password
for {}</source>
        <translation>Не могу залогиниться.
Проверьте:
   логин
   Email сервер
   или пароль
для {}</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="857"/>
        <source>{} total: {}, new: {}</source>
        <translation>{} всего: {}, новых: {}</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="865"/>
        <source>{} new: {}</source>
        <translation>{} новых: {}</translation>
    </message>
    <message>
        <location filename="../Gui/fMain.py" line="874"/>
        <source>Emails have been checked.</source>
        <translation>Почта проверена.</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="96"/>
        <source>Check Emails every</source>
        <translation>Проверять каждые</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="117"/>
        <source> minute(s)</source>
        <translation> минут(ы)</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="173"/>
        <source>Hide to tray on startup</source>
        <translation>Свернуть в трей при запуске</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="204"/>
        <source>Show pop-up:</source>
        <translation>Показать всплывающее уведомление:</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="224"/>
        <source>Bottom right</source>
        <translation>Внизу справа</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="229"/>
        <source>Bottom left</source>
        <translation>Внизу слева</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="234"/>
        <source>Top right</source>
        <translation>Вверху справа</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="239"/>
        <source>Top left</source>
        <translation>Вверху слева</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="252"/>
        <source>Disable pop-up notifications</source>
        <translation>Отключить всплывающие уведомления</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="264"/>
        <source>Disable sound notifications</source>
        <translation>Отключить звуковые уведомления</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="694"/>
        <source>Add new Emailbox</source>
        <translation>Добавить почтовый ящик</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="306"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="706"/>
        <source>Delete selected Emailboxes</source>
        <translation>Удалить выбранные ящики</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="343"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="384"/>
        <source>Delete ALL</source>
        <translation>Удалить ВСЕ</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="718"/>
        <source>Edit selected Emailbox</source>
        <translation>Редактировать выбранный ящик</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="421"/>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="730"/>
        <source>Check all Emailboxes</source>
        <translation>Проверить все почтовые ящики</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="458"/>
        <source>Check</source>
        <translation>Проверить</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="850"/>
        <source>Check selected Emailboxes</source>
        <translation>Проверить выбранные почтовые ящики</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="495"/>
        <source>Check selected</source>
        <translation>Проверить выбранные</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="838"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="626"/>
        <source>&amp;Language</source>
        <translation>&amp;Язык</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="633"/>
        <source>&amp;Help</source>
        <translation>&amp;Помощь</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="742"/>
        <source>Hide program to tray</source>
        <translation>Свернуть программу в трей</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="751"/>
        <source>About</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="754"/>
        <source>About program</source>
        <translation>О программе</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="766"/>
        <source>Русский</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="781"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="790"/>
        <source>About Qt</source>
        <translation>О Qt</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="814"/>
        <source>Hide app to tray</source>
        <translation>Свернуть программу в трей</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="826"/>
        <source>Show app</source>
        <translation>Открыть программу</translation>
    </message>
    <message>
        <location filename="../Gui/fMainUI.ui" line="862"/>
        <source>Login chosen Emailbox</source>
        <translation>Залогиниться</translation>
    </message>
</context>
</TS>
