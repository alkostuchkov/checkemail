<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>dlgAddEmbx</name>
    <message>
        <location filename="dlgAddEmailboxUI.ui" line="38"/>
        <source>Add Emailbox</source>
        <translation>Добавить почтовый ящик</translation>
    </message>
    <message>
        <location filename="dlgAddEmailbox.py" line="54"/>
        <source>You must enter full Email address.</source>
        <translation>Вы должны ввести полный Email адрес.</translation>
    </message>
    <message>
        <location filename="dlgAddEmailbox.py" line="59"/>
        <source>You must enter password.</source>
        <translation>Вы должны ввести пароль.</translation>
    </message>
    <message>
        <location filename="dlgAddEmailbox.py" line="65"/>
        <source>Check Emails</source>
        <translation>Проверка почты</translation>
    </message>
    <message>
        <location filename="dlgAddEmailbox.py" line="65"/>
        <source>Check Email task is running.
You can&apos;t add an Emailbox
untill the task is running.
Wait untill the task will finish and try again.</source>
        <translation>Запущена проверка почты.
Вы не можете добавить почтовый ящик
пока запущена проверка.
Дождитесь окончания и попробуйте снова.</translation>
    </message>
    <message>
        <location filename="dlgAddEmailbox.py" line="75"/>
        <source>Opening database</source>
        <translation>Открытие базы данных</translation>
    </message>
    <message>
        <location filename="dlgAddEmailbox.py" line="75"/>
        <source>Database access error.</source>
        <translation>Ошибка открытия базы данных.</translation>
    </message>
    <message>
        <location filename="dlgAddEmailbox.py" line="102"/>
        <source>Inserting data into DB</source>
        <translation>Добавление данных</translation>
    </message>
    <message>
        <location filename="dlgAddEmailbox.py" line="98"/>
        <source>Error inserting Emailbox.</source>
        <translation>Ошибка добаления данных.</translation>
    </message>
    <message>
        <location filename="dlgAddEmailbox.py" line="102"/>
        <source>Email was successfully added.</source>
        <translation>Email был успешно добавлен.</translation>
    </message>
    <message>
        <location filename="dlgAddEmailboxUI.ui" line="50"/>
        <source>Email:</source>
        <translation>Email:</translation>
    </message>
    <message>
        <location filename="dlgAddEmailboxUI.ui" line="74"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="dlgAddEmailboxUI.ui" line="120"/>
        <source>Show password</source>
        <translation>Показать пароль</translation>
    </message>
    <message>
        <location filename="dlgAddEmailboxUI.ui" line="131"/>
        <source>Email server: </source>
        <translation>Email сервер:</translation>
    </message>
    <message>
        <location filename="dlgAddEmailboxUI.ui" line="154"/>
        <source>google</source>
        <translation>google</translation>
    </message>
    <message>
        <location filename="dlgAddEmailboxUI.ui" line="164"/>
        <source>yandex</source>
        <translation>yandex</translation>
    </message>
    <message>
        <location filename="dlgAddEmailboxUI.ui" line="174"/>
        <source>mailru</source>
        <translation>mailru</translation>
    </message>
    <message>
        <location filename="dlgAddEmailboxUI.ui" line="184"/>
        <source>rambler</source>
        <translation>rambler</translation>
    </message>
    <message>
        <location filename="dlgAddEmailboxUI.ui" line="214"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="dlgAddEmailboxUI.ui" line="225"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
</TS>
