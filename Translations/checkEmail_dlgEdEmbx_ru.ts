<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>dlgEditEmbx</name>
    <message>
        <location filename="dlgEditEmailbox.py" line="58"/>
        <source>Opening database</source>
        <translation>Открытие базы данных</translation>
    </message>
    <message>
        <location filename="dlgEditEmailbox.py" line="58"/>
        <source>Database access error.</source>
        <translation>Ошибка открытия базы данных.</translation>
    </message>
    <message>
        <location filename="dlgEditEmailboxUI.ui" line="38"/>
        <source>Edit Emailbox</source>
        <translation>Редактирование</translation>
    </message>
    <message>
        <location filename="dlgEditEmailbox.py" line="94"/>
        <source>You must enter full Email address.</source>
        <translation>Вы должны ввести полный Email адрес.</translation>
    </message>
    <message>
        <location filename="dlgEditEmailbox.py" line="99"/>
        <source>You must enter password.</source>
        <translation>Вы должны ввести пароль.</translation>
    </message>
    <message>
        <location filename="dlgEditEmailbox.py" line="106"/>
        <source>Check Emails</source>
        <translation>Проверка почты</translation>
    </message>
    <message>
        <location filename="dlgEditEmailbox.py" line="106"/>
        <source>Check Email task is running.
You can&apos;t edit an Emailbox
untill the task is running.
Wait untill the task will finish and try again.</source>
        <translation>Запущена проверка почты.
Вы не можете редактировать почтовый ящик
пока запущена проверка.
Дождитесь окончания и попробуйте снова.</translation>
    </message>
    <message>
        <location filename="dlgEditEmailbox.py" line="126"/>
        <source>Inserting data into DB</source>
        <translation>Добавление данных</translation>
    </message>
    <message>
        <location filename="dlgEditEmailbox.py" line="122"/>
        <source>Error inserting Emailbox.</source>
        <translation>Ошибка добаления данных.</translation>
    </message>
    <message>
        <location filename="dlgEditEmailbox.py" line="126"/>
        <source>Emailbox was successfully edited.</source>
        <translation>Email был успешно отредактирован.</translation>
    </message>
    <message>
        <location filename="dlgEditEmailboxUI.ui" line="50"/>
        <source>Email:</source>
        <translation>Email:</translation>
    </message>
    <message>
        <location filename="dlgEditEmailboxUI.ui" line="80"/>
        <source>Password:</source>
        <translation>Пароль:</translation>
    </message>
    <message>
        <location filename="dlgEditEmailboxUI.ui" line="126"/>
        <source>Show password</source>
        <translation>Показать пароль</translation>
    </message>
    <message>
        <location filename="dlgEditEmailboxUI.ui" line="137"/>
        <source>Email server: </source>
        <translation>Email сервер:</translation>
    </message>
    <message>
        <location filename="dlgEditEmailboxUI.ui" line="160"/>
        <source>google</source>
        <translation>google</translation>
    </message>
    <message>
        <location filename="dlgEditEmailboxUI.ui" line="170"/>
        <source>yandex</source>
        <translation>yandex</translation>
    </message>
    <message>
        <location filename="dlgEditEmailboxUI.ui" line="180"/>
        <source>mailru</source>
        <translation>mailru</translation>
    </message>
    <message>
        <location filename="dlgEditEmailboxUI.ui" line="190"/>
        <source>rambler</source>
        <translation>rambler</translation>
    </message>
    <message>
        <location filename="dlgEditEmailboxUI.ui" line="220"/>
        <source>Edit</source>
        <translation>Редактировать</translation>
    </message>
    <message>
        <location filename="dlgEditEmailboxUI.ui" line="231"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
</TS>
