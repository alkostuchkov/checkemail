# CheckEmail
Lightweight app for checking emails.

## Name
CheckEmail

## Description
Lightweight app for checking emails.

## Installation

## Usage

## Support
alkostuchkov@gmail.com

## Roadmap
Ideas for releases in the future, it is a good idea to list them in the README.

## Contributing

## Authors and acknowledgment
Alexander Kostuchkov  
alkostuchkov@gmail.com

## License
GNU GPL v3

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
