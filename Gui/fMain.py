#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
This file is part of CheckEmail.

    CheckEmail is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    CheckEmail is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with CheckEmail.  If not, see <https://www.gnu.org/licenses/>.

  (Этот файл — часть CheckEmail.

   CheckEmail - свободная программа: вы можете перераспространять ее и/или
   изменять ее на условиях Стандартной общественной лицензии GNU в том виде,
   в каком она была опубликована Фондом свободного программного обеспечения;
   либо версии 3 лицензии, либо (по вашему выбору) любой более поздней
   версии.

   CheckEmail распространяется в надежде, что она будет полезной,
   но БЕЗО ВСЯКИХ ГАРАНТИЙ; даже без неявной гарантии ТОВАРНОГО ВИДА
   или ПРИГОДНОСТИ ДЛЯ ОПРЕДЕЛЕННЫХ ЦЕЛЕЙ. Подробнее см. в Стандартной
   общественной лицензии GNU.

   Вы должны были получить копию Стандартной общественной лицензии GNU
   вместе с этой программой. Если это не так, см.
   <https://www.gnu.org/licenses/>.)
"""

from PyQt5 import QtWidgets, QtCore, QtGui
from Gui import dlgAddEmailbox, dlgEditEmailbox, fMainUI, dlgAbout, dlgPopupMsg
from MyClasses import cEmailbox
import sqlite3
import os
from ThreadsClasses import cCheckEmailbox, cCheckInternet, cLogin
from MyExceptions import myExceptions


class MainWindow(QtWidgets.QMainWindow):
    """ Main Class """
    # My SIGNALs
    # for passing data when btnEditEmailbox clicked
    editEmbxCalled = QtCore.pyqtSignal(str, str, str)
    # when internet connection checked
    internetStatusChanged = QtCore.pyqtSignal()
    # for blocking Adding, Editing and Deleting when any checking is running
    checkingEmbxIsRun = QtCore.pyqtSignal(bool)
    # for dlgPopupMsg
    dlgPopupMsgCalled = QtCore.pyqtSignal(list, str)

    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent)
        self.ui = fMainUI.Ui_fMain()
        self.ui.setupUi(self)

        # for Translation
        self._fMainTranslator = QtCore.QTranslator()
        self._dlgAddEmbxTranslator = QtCore.QTranslator()
        self._dlgEdEmbxTranslator = QtCore.QTranslator()
        self._dlgAboutTranslator = QtCore.QTranslator()
        self._dlgPopupMsgTranslator = QtCore.QTranslator()
        self._qtbasetranslator = QtCore.QTranslator()
        self._translate = QtCore.QCoreApplication.translate
        # self.__translator = QtCore.QTranslator()

        # indexes and positions for popupMsg
        self._positionPopupMsgDict = {
            0: "Bottom right",
            1: "Bottom left",
            2: "Top right",
            3: "Top left"
        }

        # flashIconTimer and checkEveryTimer in the main thread (process)
        self._tmrFlashIcon = QtCore.QTimer(self)
        self._tmrFlashIconInterval = 200

        self._tmrCheckEvery = QtCore.QTimer(self)
        self._tmrCheckEveryInterval = 1 * 60 * 1000  # default: 60000 msec = 1 min
        # self.__remainingTimeTmrCheckEvery = self._tmrCheckEvery.remainingTime()

        # FLAGS
        # to avoid thread race between checkEmbxThread and checkSelectedEmbxThread
        self._isAnyCheckThreadRunning = False
        self._isFirstStart = True
        self._isInternet = False
        self._isNewEmails = False
# todo: delete
        # for restoring status of tmrCheckEvery
        # self.__wasCheckStarted = False
        self._tmrFlashIconWasActive = False
        # don't show annoying messageS
        # No Internet connection.\nCannot connect to IMAP4 server.
        self._isConnectionErrorInResultDict = False

        # checkInternetTimer in the main thread,
        # but checkInternet in the another (QRunnable)
        self._tmrCheckInternet = QtCore.QTimer(self)
        self._tmrCheckInternetInterval = 2000
        self._threadPool = QtCore.QThreadPool()

        # for dlgPopupMsg
        self._popupMsg = False

        # self._threadPoolLogin = QtCore.QThreadPool(self)
        # self._threadLogin = False

        # context menu for lwEmailboxes
        self._contextMenu = QtWidgets.QMenu(self)

        # SystemTrayIcon and its context menu
        self._sysTrIcon = QtWidgets.QSystemTrayIcon(self)
        self._trayMenu = QtWidgets.QMenu(self)
        # for changing system tray icon
        self._isNewEmailIcon = False

        # string for output in SystemTrayIcon's message
        self._infoAboutCheckedEmailsStr = self._translate("fMain", "Emails haven't been checked yet.")
        # list for output StatusBar message: fullEmail total new
        self._infoAboutEachCheckedEmbxsList = []

        # list for dlgPopupMsg: fullEmail new
        self._infoForDlgPopupMsgList = []

        # for pass to checkEmbxs ands checkSelectedEmbxs
        self._selectedEmbxsList = []
        # list of all lwEmbxs items
        self._lwEmbxsItemsList = []

        # change cwd to where the program exe is
        # because I DON'T KNOW why the program starts from:
        # C:\Windows\system32 on Windows or /home/username/ on Linux!!!
        self.pathToExe = os.path.abspath(sys.argv[0])
        os.chdir(os.path.dirname(self.pathToExe))

        self._settings = QtCore.QSettings("config.ini", QtCore.QSettings.IniFormat)

        # loadSetting must be above createTrayIcon because of __isAppInTray
        # is has been loaded in loadSettings and then checked in createTrayIcon!
        self.readSettings()     # 1
        self.createTrayIcon()   # 2
        self.createStatusBar()
        self.conConnections()
        self.installMyEventFilters()
        self.updateMappingEmbxs()

        # hide to tray or show on startup
        self.hide() if self.ui.chbxHideToTray.isChecked() else self.show()

        # start _tmrCheckInternet
        self._tmrCheckInternet.start(self._tmrCheckInternetInterval)
        # checking starts when tmrCheckEvery_timeout is timeouting

    def __del__(self):
        """ Write settings in destructor """
        self.writeSettings()
        self._settings.sync()

    def showEvent(self, e):
        """ Move main window to center on showEvent. """
        self.moveWindowToCenter()

    def moveWindowToCenter(self):
        """ Move main window to center. """
        screenWidth = QtWidgets.QDesktopWidget().availableGeometry().width()
        screenHeight = QtWidgets.QDesktopWidget().availableGeometry().height()
        x = (screenWidth - self.width()) // 2
        y = (screenHeight - self.height()) // 2
        self.move(x, y)

    def conConnections(self):
        self.ui.actAddEmbx.triggered.connect(self.actAddEmbx_triggered)
        self.ui.btnAddEmbx.clicked.connect(self.actAddEmbx_triggered)

        self.ui.actEdSelectedEmbx.triggered.connect(self.actEdSelectedEmbx_triggered)
        self.ui.btnEdSelectedEmbx.clicked.connect(self.actEdSelectedEmbx_triggered)

        self.ui.actDelSelectedEmbxs.triggered.connect(self.actDelSelectedEmbxs_triggered)
        self.ui.btnDelSelectedEmbxs.clicked.connect(self.actDelSelectedEmbxs_triggered)

        self.ui.actDelAllEmbxs.triggered.connect(self.actDelAllEmbxs_clicked)
        self.ui.btnDelAllEmbxs.clicked.connect(self.actDelAllEmbxs_clicked)

        self.ui.actCheckEmbxs.triggered.connect(self.actCheckEmbxs_triggered)
        self.ui.btnCheckEmbxs.clicked.connect(self.actCheckEmbxs_triggered)
        # for reset tmrCheckEvery
        self.ui.actCheckEmbxs.triggered.connect(self.btnCheckEmbxs_clicked)
        self.ui.btnCheckEmbxs.clicked.connect(self.btnCheckEmbxs_clicked)

        self.ui.actCheckSelectedEmbxs.triggered.connect(self.actCheckSelectedEmbxs_triggered)
        self.ui.btnCheckSelectedEmbxs.clicked.connect(self.actCheckSelectedEmbxs_triggered)
        # for reset tmrCheckEvery
        self.ui.actCheckSelectedEmbxs.triggered.connect(self.btnCheckSelectedEmbxs_clicked)
        self.ui.btnCheckSelectedEmbxs.clicked.connect(self.btnCheckSelectedEmbxs_clicked)

        self.ui.actLangEng.triggered.connect(self.actLangEng_triggered)
        self.ui.actLangRus.triggered.connect(self.actLangRus_triggered)

        self.ui.actAbout.triggered.connect(self.actAbout_triggered)
        self.ui.actAboutQt.triggered.connect(QtWidgets.qApp.aboutQt)

        self.ui.actShow.triggered.connect(self.setActionHideEnabled)
        self.ui.actHide.triggered.connect(self.setActionShowEnabled)
        self.ui.actHideToTray.triggered.connect(self.setActionShowEnabled)
        self.ui.actShow.triggered.connect(self.show)
        self.ui.actHide.triggered.connect(self.hide)
        self.ui.actHideToTray.triggered.connect(self.hide)
        self.ui.btnQuit.clicked.connect(self.actQuitApp_triggered)
        self.ui.actQuitApp.triggered.connect(self.actQuitApp_triggered)

        self._sysTrIcon.activated.connect(self.sysTrayIcon_activated)
        # self._sysTrIcon.messageClicked.connect(self.sysTrayIcon_msgClicked)

        self.ui.lwEmbxs.itemSelectionChanged.connect(self.lwEmbxs_itemSelectionChanged)
        self.ui.spbxCheckEvery.valueChanged.connect(self.spbxCheckEvery_valueChanged)

        # timers
        self._tmrCheckEvery.timeout.connect(self.tmrCheckEvery_timeout)
        self._tmrCheckInternet.timeout.connect(self.tmrCheckInternet_timeout)
        self.internetStatusChanged.connect(self.internetStatusChanged_changed)

        self.ui.chbxStartWithSystem.clicked.connect(self.chbxStartWithSystem_clicked)

        self.ui.actLogin.triggered.connect(self.actLogin_triggered)

    @QtCore.pyqtSlot()
    def actLogin_triggered(self):
        """ Login chosen Emailbox. """
        if len(self.ui.lwEmbxs.selectedItems()) > 1:
            QtWidgets.QMessageBox.warning(self,
            self._translate("fMain", "Login Emailbox"),
            self._translate("fMain", "You cannot login more than one Emailbox at the same time\nChoose one."))
        else:
            # check if the record is selected
            totalEmailboxes = self.ui.lwEmbxs.count()
            if totalEmailboxes == 0:  # lwEmbxs is empty.
                QtWidgets.QMessageBox.warning(self,
                self._translate("fMain", "Login Emailbox"),
                self._translate("fMain", "Nothing to login.\nAdd any Emailbox first."))
            elif self.ui.lwEmbxs.currentRow() == -1:  # or no selection.
                QtWidgets.QMessageBox.warning(self,
                self._translate("fMain", "Login Emailbox"),
                self._translate("fMain", "Nothing to login.\nChoose one."))
                self.ui.lwEmbxs.setFocus()
            else:  # is selected.
                fullEmail = self.ui.lwEmbxs.currentItem().text()
                self.login(fullEmail)
                # myCheckEmbx = self.__openDBandGetCEmailbox()
                # dCheckEmbx = myCheckEmbx.getCheckEmailboxDict()
                # serverName = dCheckEmbx[fullEmail][0]
                # password = dCheckEmbx[fullEmail][1]
                # # create login thread and run its
                # # allows to run several login at the same time!!!
                # _threadPoolLogin = QtCore.QThreadPool(self)
                # _login = cLogin.CLogin(fullEmail, password, serverName)
                # _login.signals.error.connect(self.loginError_occured)
                #
                # _threadPoolLogin.start(_login)

    @QtCore.pyqtSlot(str)
    def login(self, fullEmail):
        """ Login Emailbox. """
        myCheckEmbx = self.__openDBandGetCEmailbox()
        dCheckEmbx = myCheckEmbx.getCheckEmailboxDict()
        serverName = dCheckEmbx[fullEmail][0]
        password = dCheckEmbx[fullEmail][1]
        # create login thread and run its
        # allows to run several login at the same time!!!
        _threadPoolLogin = QtCore.QThreadPool(self)
        _login = cLogin.CLogin(fullEmail, password, serverName)
        _login.signals.error.connect(self.loginError_occured)

        _threadPoolLogin.start(_login)

    @QtCore.pyqtSlot(tuple)
    def loginError_occured(self, err):
        """ Show message when an loginError occured. """
        if isinstance(err[0], FileNotFoundError):
            QtWidgets.QMessageBox.warning(self,
            self._translate("fMain", "Login Emailbox"),
            self._translate("fMain", "Cannot login.\n{}".format(err[0])))
        elif isinstance(err[0], myExceptions.MyEmailboxLoginError):
            QtWidgets.QMessageBox.warning(self,
            self._translate("fMain", "Login Emailbox"),
            self._translate("fMain", "Cannot login.\n{}".format(err[0])))
        elif isinstance(err[0], myExceptions.MyBadBrowserOrOSError):
            QtWidgets.QMessageBox.warning(self,
            self._translate("fMain", "Login Emailbox"),
            self._translate("fMain", "Unknown browser.\n"
                            "Program supports only\n"
                            "Firefox, Opera or Chrome browsers."))

    @QtCore.pyqtSlot()
    def chbxStartWithSystem_clicked(self):
        """ Add or remove app to/from system autorun """
        appName = "CheckEmail"
        if os.name == "nt":  # for Windows
            import winreg
            subKey = r"SOFTWARE\Microsoft\Windows\CurrentVersion\Run"
            value = '"' + self.pathToExe + '"'
            try:
                key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, subKey, reserved=0, access=winreg.KEY_ALL_ACCESS)
            except Exception as err:
                QtWidgets.QMessageBox.warning(self,
                self._translate("fMain", "Start with system"),
                self._translate("fMain", "Cannot add to autorun\n{}").format(err))
            else:
                if self.ui.chbxStartWithSystem.isChecked():
                    try:
                        winreg.SetValueEx(key, "CheckEmail", 0, winreg.REG_SZ, value)
                    except Exception as err:
                        QtWidgets.QMessageBox.warning(self,
                        self._translate("fMain", "Start with system"),
                        self._translate("fMain", "Cannot add to autorun\n{}").format(err))
                else:
                    try:
                        winreg.DeleteValue(key, "CheckEmail")
                    except Exception as err:
                        QtWidgets.QMessageBox.warning(self,
                        self._translate("fMain", "Start with system"),
                        self._translate("fMain", "Cannot delete from autorun\n{}").format(err))
            finally:
                winreg.CloseKey(key)
        elif os.name == "posix":  # for Linux systems
            homeDir = os.environ["HOME"]
            pathToIcon = self.pathToExe + ".ico"
            pathToAutorunDir = "{}/.config/autostart/".format(homeDir)
            pathToAppDir = os.path.dirname(self.pathToExe) + os.path.sep
            pathToAutorunFiledesktop = pathToAutorunDir + "CheckEmail.desktop"         
            autorunFileText = "[Desktop Entry]\n" \
                              "Name={}\n" \
                              "Encoding=UTF-8\n" \
                              "Comment=Light weight app for monitoring new Emails\n" \
                              "Path={}\n" \
                              "Exec={}\n" \
                              "Icon={}\n" \
                              "Terminal=false\n" \
                              "Type=Application\n" \
                              "Categories = Network;\n" \
                              "StartupNotify=false\n" \
                              "Hidden=false".format(appName, pathToAppDir, self.pathToExe, pathToIcon)

            if not os.path.exists(pathToAutorunDir):
                QtWidgets.QMessageBox.warning(self,
                self._translate("fMain", "Start with system"),
                self._translate("fMain", "{} path doesn't exist.\n"
                                         "Cannot add to autorun").format(pathToAutorunDir))
            else:
                if os.path.isfile(pathToAutorunFiledesktop):
                    os.remove(pathToAutorunFiledesktop)
                if self.ui.chbxStartWithSystem.isChecked():
                    try:
                        myFile = open(pathToAutorunFiledesktop, "w")
                        myFile.write(autorunFileText)
                        # os.system("chmod +x {}".format(autorunFileDesktop))
                    except Exception as err:
                        QtWidgets.QMessageBox.warning(self,
                        self._translate("fMain", "Start with system"),
                        self._translate("fMain", "Creating autorun file error.\n"
                                                 "Cannot add to autorun").format(err))
                    finally:
                        myFile.close()

    def createContextMenu(self):
        """ Context menu for lwEmailboxes """
        self._contextMenu.addAction(self.ui.actAddEmbx)
        self._contextMenu.addAction(self.ui.actDelSelectedEmbxs)
        self._contextMenu.addAction(self.ui.actEdSelectedEmbx)
        self._contextMenu.addSeparator()
        self._contextMenu.addAction(self.ui.actCheckEmbxs)
        self._contextMenu.addAction(self.ui.actCheckSelectedEmbxs)
        self._contextMenu.addSeparator()
        self._contextMenu.addAction(self.ui.actLogin)
        self._contextMenu.addSeparator()
        self._contextMenu.addAction(self.ui.actAbout)
        self._contextMenu.addAction(self.ui.actAboutQt)
        self._contextMenu.addSeparator()
        self._contextMenu.addAction(self.ui.actHideToTray)
        self._contextMenu.addAction(self.ui.actQuitApp)

    def createTrayIcon(self):
        """ Context trayMenu and SystemTrayIcon """
        self._trayMenu.addAction(self.ui.actShow)
        self._trayMenu.addAction(self.ui.actHide)
        self._trayMenu.addSeparator()
        self._trayMenu.addAction(self.ui.actCheckEmbxs)
        # self._trayMenu.addSeparator()
        # self._trayMenu.addAction(self.ui.actLogin)
#todo: show info about received Emails
        # self._trayMenu.addAction(showPopupWindowAboutEmails)
        self._trayMenu.addSeparator()
        self._trayMenu.addAction(self.ui.actQuitApp)

        # change actions Show and Hide status in context menu sysTrayIcon
        if self.ui.chbxHideToTray.isChecked():
            self.ui.actShow.setEnabled(True)
            self.ui.actHide.setEnabled(False)
        else:
            self.ui.actShow.setEnabled(False)
            self.ui.actHide.setEnabled(True)

        self._sysTrIcon.setToolTip(self._translate("fMain", "Check Emailbox"))
        self._sysTrIcon.setContextMenu(self._trayMenu)
        self._sysTrIcon.setIcon(QtGui.QIcon(":/icons/Icons/CheckEmail.ico"))
        self._sysTrIcon.show()

    def installMyEventFilters(self):
        """  Install my event filters """
        self.ui.lwEmbxs.installEventFilter(self)

    def createStatusBar(self):
        """  StatusBar """
        self.ui.prBarCheckingEmbx.setVisible(False)
        self.ui.statusBar.addWidget(self.ui.lblStatusBarOutput, 4)
        self.ui.statusBar.addWidget(self.ui.prBarCheckingEmbx, 1)

    def writeSettings(self):
        """ Store program setting to config.ini file """
        self._settings.beginGroup("LanguageMenu")
        dActions = dict()
        for action in self.ui.menu_Options.actions():
            if action.isCheckable():
                dActions.setdefault(action.text(), action.isChecked())
        self._settings.setValue("LanguageMenuItems", dActions)
        self._settings.endGroup()

        self._settings.beginGroup("States")
        self._settings.setValue("WindowGeometry", self.saveGeometry())
        self._settings.setValue("SplitterHorizontal", self.ui.splHorizontal.saveState())
        self._settings.setValue("SplitterVertical", self.ui.splVertical.saveState())
        self._settings.endGroup()

        self._settings.beginGroup("Check")
        self._settings.setValue("CheckEmailsEvery", self.ui.spbxCheckEvery.value())
        self._settings.setValue("StartWithSystem", self.ui.chbxStartWithSystem.isChecked())
        self._settings.setValue("HideToTrayOnStartup", self.ui.chbxHideToTray.isChecked())
        self._settings.setValue("DisablePopupNotifications", self.ui.chbxDisablePopup.isChecked())
        self._settings.setValue("DisableSoundNotifications", self.ui.chbxDisableSound.isChecked())
        self._settings.setValue("WhereShowPopupMessage", self.ui.cbxWhereShowPopup.currentIndex())
        self._settings.endGroup()

    def readSettings(self):
        """ Restore program settings from file """
        self._settings.beginGroup("LanguageMenu")
        dActions = self._settings.value("LanguageMenuItems")
        if dActions != None:  # config.ini doesn't exist
            for action in self.ui.menu_Options.actions():
                if action.isCheckable():
                    bIsChecked = dActions.get(action.text(), False)
                    if bIsChecked:  # set checked language
                        action.setChecked(bIsChecked)
                        if action.text() == "English":
                            self.actLangEng_triggered()
                        elif action.text() == "Русский":
                            self.actLangRus_triggered()
                        break
        self._settings.endGroup()

        self._settings.beginGroup("States")
        windowGeometry = self._settings.value("WindowGeometry")
        if windowGeometry != None:
            self.restoreGeometry(windowGeometry)
        splHorizontalDefault = br'\0\0\0\xff\0\0\0\x1\0\0\0\x2\0\0\x2p\0\0\0\xbc\x1\xff\xff\xff\xff\x1\0\0\0\x1\0'
        self.ui.splHorizontal.restoreState(self._settings.value("SplitterHorizontal", splHorizontalDefault))
        splVerticalDefault = br'\0\0\0\xff\0\0\0\x1\0\0\0\x2\0\0\0\xc0\0\0\0\xe1\x1\xff\xff\xff\xff\x1\0\0\0\x2\0'
        self.ui.splVertical.restoreState(self._settings.value("SplitterVertical", splVerticalDefault))
        self._settings.endGroup()

        self._settings.beginGroup("Check")
        self.ui.spbxCheckEvery.setValue(self._settings.value("CheckEmailsEvery", 1, type=int))
        self.ui.chbxStartWithSystem.setChecked(self._settings.value("StartWithSystem", False, type=bool))
        self.ui.chbxHideToTray.setChecked(self._settings.value("HideToTrayOnStartup", False, type=bool))
        self.ui.chbxDisablePopup.setChecked(self._settings.value("DisablePopupNotifications", False, type=bool))
        self.ui.chbxDisableSound.setChecked(self._settings.value("DisableSoundNotifications", False, type=bool))
        self.ui.cbxWhereShowPopup.setCurrentIndex(self._settings.value("WhereShowPopupMessage", 0, type=int))
        self._settings.endGroup()

        self._tmrCheckEveryInterval = self.ui.spbxCheckEvery.value() * 60 * 1000  # msec

    def __openDBandGetCEmailbox(self):
        """
        To avoid duplicate code:
        try to create instance of CEmailbox and get its.
        :return: CEmailbox instance
        """
        myCheckEmbx = cEmailbox.CEmailbox()
        try:
            myCheckEmbx.openDBandGetDict()
        except sqlite3.DatabaseError:
            QtWidgets.QMessageBox.critical(self,
            self._translate("fMain", "Opening database"),
            self._translate("fMain", "Database access error."))
        else:
            return myCheckEmbx

    def updateMappingEmbxs(self):
        """ Update view in lwEmbxs after changes """
        myCheckEmbx = self.__openDBandGetCEmailbox()
        dCheckEmailbox = myCheckEmbx.getCheckEmailboxDict()
        self.ui.lwEmbxs.clear()
        self._lwEmbxsItemsList.clear()
        if not myCheckEmbx.getIsDbEmpty():
            for key in sorted(dCheckEmailbox):
                self.ui.lwEmbxs.addItem(key)
                # store content of lwEmbxs for lwEmbxs_itemSelectionChanged
                self._lwEmbxsItemsList.append(key)

#todo: popup window instead SystemTrayIconMessage
# addEmbxDlg.setWindowFlag(QtCore.Qt.Popup, True)

    @QtCore.pyqtSlot()
    def actAddEmbx_triggered(self):
        """ Adding new Emailbox """
        addEmbxDlg = dlgAddEmailbox.DlgAddEmail()
        # for blocking Adding when any checking is running
        self.checkingEmbxIsRun.connect(addEmbxDlg.setIsAnyCheckThreadRunning)
        self.checkingEmbxIsRun.emit(self._isAnyCheckThreadRunning)
        addEmbxDlg.exec_()
        self.updateMappingEmbxs()

    @QtCore.pyqtSlot()
    def actEdSelectedEmbx_triggered(self):
        """ Edit selected Emailbox (one) """
        if len(self.ui.lwEmbxs.selectedItems()) > 1:
            QtWidgets.QMessageBox.warning(self,
            self._translate("fMain", "Edit Emailbox"),
            self._translate("fMain", "You cannot edit more than one Emailbox at the same time\nChoose one."))
        else:
            # check if the record is selected
            totalEmailboxes = self.ui.lwEmbxs.count()
            if totalEmailboxes == 0:  # lwEmbxs is empty.
                QtWidgets.QMessageBox.warning(self,
                self._translate("fMain", "Edit Emailbox"),
                self._translate("fMain", "Nothing to edit.\nAdd any Emailbox first."))
            elif self.ui.lwEmbxs.currentRow() == -1:  # or no selection.
                QtWidgets.QMessageBox.warning(self,
                self._translate("fMain", "Edit Emailbox"),
                self._translate("fMain", "Nothing to edit.\nChoose one."))
                self.ui.lwEmbxs.setFocus()
            else:  # is selected.
                editEmbxDlg = dlgEditEmailbox.DlgEditEmailbox()
                # emit SIGNAL btnEditEmailboxCalled (pass: fullEmail, password, serverNameIndex)
                fullEmail = self.ui.lwEmbxs.currentItem().text()
                # get password and serverNameIndex
                myCheckEmbx = self.__openDBandGetCEmailbox()
                dCheckEmailbox = myCheckEmbx.getCheckEmailboxDict()
                password = dCheckEmailbox[fullEmail][1]
                serverName = dCheckEmailbox[fullEmail][0]
                if serverName == "imap.mail.ru":
                    serverName = "mailru"
                elif serverName == "imap.gmail.com":
                    serverName = "google"
                elif serverName == "imap.yandex.com":
                    serverName = "yandex"
                elif serverName == "imap.rambler.ru":
                    serverName = "rambler"
                # connect SIGNAL btnEditEmailboxCalled with SLOT
                self.editEmbxCalled.connect(editEmbxDlg.fMain_btnEdEmbxCalled)
                self.editEmbxCalled.emit(fullEmail, serverName, password)
                # for blocking Edit when any checking is running
                self.checkingEmbxIsRun.connect(editEmbxDlg.setIsAnyCheckThreadRunning)
                self.checkingEmbxIsRun.emit(self._isAnyCheckThreadRunning)
                editEmbxDlg.exec_()
                self.updateMappingEmbxs()

    @QtCore.pyqtSlot()
    def actDelSelectedEmbxs_triggered(self):
        """ Deleting selected records from the DB """
        # check if lwEmbx is empty
        totalEmailboxes = self.ui.lwEmbxs.count()
        if totalEmailboxes == 0:
            QtWidgets.QMessageBox.warning(self,
            self._translate("fMain", "Delete Emailbox"),
            self._translate("fMain", "Nothing to delete.\nAdd any Emailbox first."))
        # check if the record is selected
        elif self.ui.lwEmbxs.currentRow() == -1:  # no selection.
            QtWidgets.QMessageBox.warning(self,
            self._translate("fMain", "Delete Emailbox"),
            self._translate("fMain", "Nothing to delete.\nFor deleting choose an Emailbox in the list."))
            self.ui.lwEmbxs.setFocus()
        else:  # is selected.
            # block access when another checking thread is running
            if self._isAnyCheckThreadRunning:
                QtWidgets.QMessageBox.warning(self,
                self._translate("fMain", "Check Emails"),
                self._translate("fMain", "Check Email task is running.\n"
                                         "You can't delete Emailboxes\nuntill the task is running.\n"
                                         "Wait untill the task will finish and try again."))
            else:
                myCheckEmbx = cEmailbox.CEmailbox()
                try:
                    myCheckEmbx.openDBandGetDict()
                except sqlite3.DatabaseError:
                    QtWidgets.QMessageBox.critical(self,
                    self._translate("fMain", "Opening database"),
                    self._translate("fMain", "Database access error."))
                else:
                    # get fullEmailsList
                    fullEmailsList = []  # list of fullEmails from selected rows in the lwEmbxs.
                    # get all selected items as QList of QListWidgetItems
                    allSelectedItems = self.ui.lwEmbxs.selectedItems()
                    for fullEmail in allSelectedItems:
                        fullEmailsList.append(fullEmail.text())

                    btnReply = QtWidgets.QMessageBox.question(self,
                    self._translate("fMain", "Delete Emailbox"),
                    self._translate("fMain", "Are you sure you want to delete selected Emailboxes?"),
                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.No)
                    if btnReply == QtWidgets.QMessageBox.Yes:
                        try:
                            myCheckEmbx.multiDeletingRecords(fullEmailsList)
                        except sqlite3.DatabaseError:
                            QtWidgets.QMessageBox.critical(self,
                            self._translate("fMain", "Delete Emailbox"),
                            self._translate("fMain", "Deleting Emailbox Error."))
                        else:  # delete from the dict CheckEmailbox._checkEmailboxDict
                            dCheckEmbx = myCheckEmbx.getCheckEmailboxDict()
                            for fullEmail in fullEmailsList:
                                del dCheckEmbx[fullEmail]
                            myCheckEmbx.setCheckEmailboxDict(dCheckEmbx)
                            QtWidgets.QMessageBox.information(self,
                            self._translate("fMain", "Delete Emailbox"),
                            self._translate("fMain", "Deleting was completed successfully."))

                            # Show all records in the Main Form after deleting records.
                            self.updateMappingEmbxs()

    @QtCore.pyqtSlot()
    def actDelAllEmbxs_clicked(self):
        """ Clear DB deleting ALL Emailboxes """
        # block access when another checking thread is running
        if self._isAnyCheckThreadRunning:
            QtWidgets.QMessageBox.warning(self,
            self._translate("fMain", "Check Emails"),
            self._translate("fMain", "Check Email task is running.\n"
                                     "You can't delete Emailboxes\nuntill the task is running.\n"
                                     "Wait untill the task will finish and try again."))
        else:
            myCheckEmbx = self.__openDBandGetCEmailbox()
            if myCheckEmbx.getIsDbEmpty():  # IS already empty.
                QtWidgets.QMessageBox.information(self,
                self._translate("fMain", "Delete all Emailboxes"),
                self._translate("fMain", "You Database is already empty."))
            else:  # IS NOT empty
                btnReply = QtWidgets.QMessageBox.critical(self,
                self._translate("fMain", "Delete All Emailboxes"),
                self._translate("fMain", "This action will delete ALL Emailboxes\nfrom Database completely.\n"
                                         "Are you sure to do this?"),
                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No, QtWidgets.QMessageBox.No)
                if btnReply == QtWidgets.QMessageBox.Yes:
                    try:
                        myCheckEmbx.clearDB()
                    except sqlite3.DatabaseError:
                        QtWidgets.QMessageBox.critical(self,
                        self._translate("fMain", "Delete All Emailboxes"),
                        self._translate("fMain", "Deleting ALL Emailboxes error."))
                    else:
                        dCheckEmbx = myCheckEmbx.getCheckEmailboxDict()
                        dCheckEmbx.clear()
                        myCheckEmbx.setCheckEmailboxDict(dCheckEmbx)
                        self.ui.lwEmbxs.clear()
                        QtWidgets.QMessageBox.information(self,
                        self._translate("fMain", "Delete All Emailboxes"),
                        self._translate("fMain", "Deleting ALL Emailboxes was completed successfully."))

                        # Show all records in the Main Form after deleting records.
                        self.updateMappingEmbxs()

    @QtCore.pyqtSlot()
    def lwEmbxs_itemSelectionChanged(self):
        """ Show in statusBar message info about Emailbox in currentRow """
        if self._isAnyCheckThreadRunning:
            self.ui.lblStatusBarOutput.setText(self._translate("fMain", "Checking. Please wait..."))
        else:
            if len(self.ui.lwEmbxs.selectedItems()) == 1:  # only one selected
                if len(self._infoAboutCheckedEmailsStr) != 0:
                    checkedList = []
                    uncheckedList = []
                    # separate all Embxs to checked & unchecked
                    for line in self._infoAboutEachCheckedEmbxsList:
                        checkedList.append(line.split()[0])
                    for fullEmail in self._lwEmbxsItemsList:
                        if fullEmail not in checkedList:
                            uncheckedList.append(fullEmail + self._translate("fMain", " has't been checked."))
                    self._infoAboutEachCheckedEmbxsList.extend(uncheckedList)

                    selectedEmbx = self.ui.lwEmbxs.currentItem().text()
                    for result in self._infoAboutEachCheckedEmbxsList:
                        if result.split()[0] == selectedEmbx:
                            self.ui.lblStatusBarOutput.setText(result)
                        else:
                            continue
            else:  # none is selected
                self.ui.lblStatusBarOutput.clear()

    @QtCore.pyqtSlot()
    def spbxCheckEvery_valueChanged(self):
        """
        Reset CheckEveryTimer:
        Stop CheckEveryTimer,
        Set new timer interval from spbxCheckEvery,
        Start CheckEveryTimer
        """
        if self._tmrCheckEvery.isActive():
            self._tmrCheckEvery.timeout.disconnect(self.tmrCheckEvery_timeout)
            self._tmrCheckEvery.stop()
            self._tmrCheckEvery.timeout.connect(self.tmrCheckEvery_timeout)
            self._tmrCheckEvery.start(self.ui.spbxCheckEvery.value() * 60 * 1000)  # msec
            self.ui.btnCheckEmbxs.setFocus()

    @QtCore.pyqtSlot()
    def btnCheckEmbxs_clicked(self):
        """
        Reset tmrCheckEvery after every checking if btnCheck or btnCheckSelected was clicked
        """
        self.spbxCheckEvery_valueChanged()
        # to avoid crash show QMessageBox only when app is visible because
        # when app is in tray, after showing QMessageBox, it's chrashed and
        # I don't know why!!!
        if not self._isInternet:  # and self.isVisible():
            QtWidgets.QMessageBox.warning(self,
            self._translate("fMain", "Check Emails"),
            self._translate("fMain", "No Internet connection."))

    @QtCore.pyqtSlot()
    def btnCheckSelectedEmbxs_clicked(self):
        """
        Reset tmrCheckEvery after every checking if btnCheck or btnCheckSelected was clicked
        """
        self.spbxCheckEvery_valueChanged()
        if not self._isInternet:
            QtWidgets.QMessageBox.warning(self,
            self._translate("fMain", "Check Emails"),
            self._translate("fMain", "No Internet connection."))

    @QtCore.pyqtSlot()
    def actCheckEmbxs_triggered(self):
        """
        Create instances of CCheckEmailbox and Thread
        and run thread
        """
        # if internet is
        if self._isInternet:
            myCheckEmbx = self.__openDBandGetCEmailbox()
            if myCheckEmbx.getIsDbEmpty():
                QtWidgets.QMessageBox.warning(self,
                self._translate("fMain", "Check Emails"),
                self._translate("fMain",
                "There are no Emailboxes in the Database.\nAdd any Emailbox first."))
            else:  # to avoid running both checking Emailboxes at the same time
                if not self._isAnyCheckThreadRunning:
                    self._isAnyCheckThreadRunning = True
# todo: delete
                    # self.__wasCheckStarted = True
                    # lock Adding, Editing and Deleting while checking is running
                    self.checkingEmbxIsRun.emit(self._isAnyCheckThreadRunning)

                    self.ui.lblStatusBarOutput.setText(self._translate("fMain", "Checking. Please wait..."))
                    self._sysTrIcon.setToolTip(self._translate("fMain", "Checking. Please wait..."))
                    self.ui.prBarCheckingEmbx.setMaximum(0)
                    self.ui.prBarCheckingEmbx.setVisible(True)

                    # if checking started but internet connection has been lost
                    # create CCheckEmailbox instance and thread
                    self.checkEmbx = cCheckEmailbox.CCheckEmailbox(myCheckEmbx)
                    self.checkEmbxThread = QtCore.QThread(self)
                    self.checkEmbx.moveToThread(self.checkEmbxThread)
                    # connect for getting resultDict
                    self.checkEmbx.emlbxsChecked.connect(self.embxsChecked_checked)
                    # do check Emailboxes when checkEmbsxThread starts (run())
                    self.checkEmbxThread.started.connect(self.checkEmbx.checkEmailbox)
                    # deleting and quit thread when job's done
                    self.checkEmbx.finished.connect(self.checkEmbxThread.quit)
                    self.checkEmbx.finished.connect(self.checkEmbx.deleteLater)
                    self.checkEmbxThread.finished.connect(self.checkEmbxThread.deleteLater)
                    if self._isInternet:
                        self.checkEmbxThread.start()
                elif self.isVisible():
                    QtWidgets.QMessageBox.warning(self,
                    self._translate("fMain", "Check Emails"),
                    self._translate("fMain",
                    "Check Email task is running.\nWait untill the task will finish and try again."))
        else:  # if no internet
            print("actCheckEmbxs_triggered => NO INTERNET")

    @QtCore.pyqtSlot()
    def actCheckSelectedEmbxs_triggered(self):
        """
        Check only selected Emailboxes.
        Create instances of CCheckEmailbox and Thread
        and run thread
        """
        # if internet is
        if self._isInternet:
            myCheckEmbx = self.__openDBandGetCEmailbox()
            if myCheckEmbx.getIsDbEmpty():
                QtWidgets.QMessageBox.warning(self,
                self._translate("fMain", "Check Emails"),
                self._translate("fMain",
                "There are no Emailboxes in the Database.\nAdd any Emailbox first."))
            else:  # to avoid running both checking Emailboxes at the same time
                if not self._isAnyCheckThreadRunning:
                    self._isAnyCheckThreadRunning = True
# todo: delete
                    # self.__wasCheckStarted = True
                    if len(self.ui.lwEmbxs.selectedItems()) == 0:
                        QtWidgets.QMessageBox.warning(self,
                        self._translate("fMain", "Check Emails"),
                        self._translate("fMain", "There is noone selected Emailbox.\nSelect at least one."))
                    else:  # to avoid running both checking Emailboxes at the same time
                        # get list of selected Emailboxes
                        selectedEmbxsList = []
                        for fullEmail in self.ui.lwEmbxs.selectedItems():
                            selectedEmbxsList.append(fullEmail.text())

                        self.ui.lblStatusBarOutput.setText(self._translate("fMain", "Checking. Please wait..."))
                        self._sysTrIcon.setToolTip(self._translate("fMain", "Checking. Please wait..."))
                        self.ui.prBarCheckingEmbx.setMaximum(0)
                        self.ui.prBarCheckingEmbx.setVisible(True)
                        # create CCheckEmailbox instance and thread
                        self.checkEmbx = cCheckEmailbox.CCheckEmailbox(myCheckEmbx, selectedEmbxsList)
                        self.checkEmbxThread = QtCore.QThread(self)
                        self.checkEmbx.moveToThread(self.checkEmbxThread)
                        # connect self.checkEmbx Signal with self Slot for getting resultDict
                        self.checkEmbx.emlbxsChecked.connect(self.embxsChecked_checked)
                        # doing check Emailboxes when checkEmbsxThread starts (thread's run())
                        self.checkEmbxThread.started.connect(self.checkEmbx.checkEmailbox)
                        # deleting and quit thread when job's done
                        self.checkEmbx.finished.connect(self.checkEmbxThread.quit)
                        self.checkEmbx.finished.connect(self.checkEmbx.deleteLater)
                        self.checkEmbxThread.finished.connect(self.checkEmbxThread.deleteLater)
                        if self._isInternet:
                            self.checkEmbxThread.start()
                else:
                    QtWidgets.QMessageBox.warning(self,
                    self._translate("fMain", "Check Emails"),
                    self._translate("fMain",
                    "Check Email task is running.\nWait untill the task will finish and try again."))
        else:  # if no internet
            print("actCheckSelectedEmbxs_triggered => NO INTERNET")

    @QtCore.pyqtSlot(dict)
    def embxsChecked_checked(self, resultDict):
        """
        Processing dict with Emails that got from thread
        Slot connected to Signal embxsChecked in CCheckEmailbox
        :param resultDict
        """
        # reset selt.__infoAboutCheckedEmails
        self._infoAboutCheckedEmailsStr = ""
        self._infoAboutEachCheckedEmbxsList.clear()
        self._infoForDlgPopupMsgList.clear()
        self._isNewEmails = False
        self._isConnectionErrorInResultDict = False
        for fullEmail in sorted(resultDict):
            if len(resultDict[fullEmail]) == 3:  # there was Error
                #todo: disable
                # for not to show annoying messageS about ConnectionError just show only one!!!
                # if not self._isConnectionErrorInResultDict and resultDict[fullEmail][2] == "ConnectionError":
                #     self._isConnectionErrorInResultDict = True
                #     QtWidgets.QMessageBox.critical(self,
                #     self._translate("fMain", "Check Emails"),
                #     self._translate("fMain", "No Internet connection.\nCannot connect to IMAP4 server."))

                # to avoid crash show QMessageBox only when app is visible because
                # when app is in tray, after showing QMessageBox, it's chrashed and
                # I don't know why!!!
                if resultDict[fullEmail][2] == "LoginError":  # and self.isVisible():
                    QtWidgets.QMessageBox.critical(self,
                    self._translate("fMain", "Check Emails"),
                    self._translate("fMain", "Cannot login.\n"
                                             "Check:\n   login\n"
                                             "   Email server\n"
                                             "   or password\nfor {}").format(fullEmail))

            # set flag if at least one new Email's received
            if (not self._isNewEmails and
                    type(resultDict[fullEmail][1]) == int and
                    resultDict[fullEmail][1] > 0):
                self._isNewEmails = True

            # output for StatusBar message: fullEmail total new
    #todo: delete if works
            # sInfoAboutCheckedEmbx = self._translate("fMain", "{} total: {}, new: {}\n").format(
            sInfoAboutCheckedEmbx = self._translate("fMain", "{} total: {}, new: {}").format(
                fullEmail, resultDict[fullEmail][0], resultDict[fullEmail][1])
    #todo: delete if works
            # self._infoAboutEachCheckedEmbxsList.append(sInfoAboutCheckedEmbx.rstrip())
            self._infoAboutEachCheckedEmbxsList.append(sInfoAboutCheckedEmbx)
            # output for TrayIcon message: fullEmail new
    #todo: delete if works
            # sInfoAboutCheckedEmbx = self._translate("fMain", "{} new: {}\n").format(
            sInfoAboutCheckedEmbx = self._translate("fMain", "{} new: {}").format(
                fullEmail, resultDict[fullEmail][1])
            self._infoForDlgPopupMsgList.append(sInfoAboutCheckedEmbx)
            self._infoAboutCheckedEmailsStr += sInfoAboutCheckedEmbx

            # for output in StatusBar
            self.ui.prBarCheckingEmbx.setMaximum(100)
            self.ui.prBarCheckingEmbx.setVisible(False)
            self._sysTrIcon.setToolTip(self._translate("fMain", "Check Emailbox"))
            self.ui.statusBar.showMessage(self._translate("fMain", "Emails have been checked."), 2000)

        # delete last \n
        self._infoAboutCheckedEmailsStr = self._infoAboutCheckedEmailsStr.rstrip()
        if self._isNewEmails:
            if self._tmrFlashIcon.isActive():
                self._tmrFlashIcon.timeout.disconnect(self.tmrFlashIcon_timeout)
                self._tmrFlashIcon.stop()
            self._tmrFlashIcon.timeout.connect(self.tmrFlashIcon_timeout)
            self._tmrFlashIcon.start(self._tmrFlashIconInterval)
            self._tmrFlashIconWasActive = True
        else:  # stop tmrFlashIcon
            if self._tmrFlashIcon.isActive():
                self._tmrFlashIcon.stop()
                self._tmrFlashIcon.timeout.disconnect(self.tmrFlashIcon_timeout)
                self._tmrFlashIconWasActive = False
                self._sysTrIcon.setIcon(QtGui.QIcon(":/icons/Icons/CheckEmail.ico"))
        # Show SysteTrayIcon message only if at least one new Email is,
        # pop-up message and sound if they are allowed
        if self._isNewEmails and not self.ui.chbxDisableSound.isChecked():
            self.playSound()
        if self._isNewEmails and not self.ui.chbxDisablePopup.isChecked():
            # imitate mouse middle button click for show SystemTrayIcon message
            self.sysTrayIcon_activated(3)

        # restore FLAGS
        self._isConnectionErrorInResultDict = False
#todo: delete
        # self.__wasCheckStarted = False
        # for ability to run checking again
        self._isAnyCheckThreadRunning = False
        # unlock Adding, Editing and Deleting while checking is running
        self.checkingEmbxIsRun.emit(self._isAnyCheckThreadRunning)
        self.lwEmbxs_itemSelectionChanged()

    @QtCore.pyqtSlot()
    def tmrFlashIcon_timeout(self):
        """ Flash icon when new Email received """
        if self._isNewEmailIcon:
            self._sysTrIcon.setIcon(QtGui.QIcon(":/icons/Icons/48x48/newmail.ico"))
            self._isNewEmailIcon = False
        else:
            self._sysTrIcon.setIcon(QtGui.QIcon(":/icons/Icons/48x48/nomail.ico"))
            self._isNewEmailIcon = True

    @QtCore.pyqtSlot()
    def tmrCheckEvery_timeout(self):
        """ Schedule checking all Emailboxes every... """
        if self._isInternet and not self._isAnyCheckThreadRunning:
            self.actCheckEmbxs_triggered()

    @QtCore.pyqtSlot()
    def tmrCheckInternet_timeout(self):
        """ Run check internet connection in another thread (QRunnable) """
        checkInternet = cCheckInternet.CCheckInternet()
        checkInternet.signals.result.connect(self.setIsInternet)
        self._threadPool.start(checkInternet)

    @QtCore.pyqtSlot(bool)
    def setIsInternet(self, isInternet):
        """ Set self._isInternet and emit SIGNAL internetStatusChanged """
        if not isInternet:
            self._sysTrIcon.setIcon(QtGui.QIcon(":/icons/Icons/128x128/noInternet.ico"))
        # emit internetStatusChanged only if it's realy changed!
        if self._isInternet != isInternet:
            self._isInternet = isInternet
            if self._isFirstStart:
                self._sysTrIcon.setIcon(QtGui.QIcon(":/icons/Icons/CheckEmail.ico"))
                self._isFirstStart = False
                self.actCheckEmbxs_triggered()
                # start _tmrCheckEvery
                self._tmrCheckEvery.start(self._tmrCheckEveryInterval)  # msec read from loadSettings
            else:
                self.internetStatusChanged.emit()

    @QtCore.pyqtSlot()
    def internetStatusChanged_changed(self):
        """
        Store/Restore previous of:
        _tmrFlashIcon (wasActive or not),
        _tmrCheckEvery,
        checkingEmailboxes
        when internetStatusChanged.
        """
        if self._isInternet:  # is InternetConnection
            # _tmrFlashIcon
            if not self._tmrFlashIcon.isActive() and not self._tmrFlashIconWasActive:
                self._sysTrIcon.setIcon(QtGui.QIcon(":/icons/Icons/CheckEmail.ico"))
            else:
                self._tmrFlashIcon.start(self._tmrFlashIconInterval)
# todo: delete
            # if self.__wasCheckStarted and not self._isAnyCheckThreadRunning:
            #     self.actCheckEmbxs_triggered()
        else:  # no InternetConnection
            # _tmrFlashIcon
            self._sysTrIcon.setIcon(QtGui.QIcon(":/icons/Icons/128x128/noInternet.ico"))
            self._sysTrIcon.setToolTip(self._translate("fMain", "Check Emailbox"))
            if self._tmrFlashIcon.isActive():
                self._tmrFlashIconWasActive = True
                self._tmrFlashIcon.stop()

            # prBarCheckingEmbx
            self.ui.prBarCheckingEmbx.setMaximum(100)
            self.ui.prBarCheckingEmbx.setVisible(False)

            # for ability to run checking again
            self._isAnyCheckThreadRunning = False
            # unlock Adding, Editing and Deleting while checking is running
            self.checkingEmbxIsRun.emit(self._isAnyCheckThreadRunning)
            self._sysTrIcon.setToolTip(self._translate("fMain", "Check Emailbox"))
            self.lwEmbxs_itemSelectionChanged()

    @QtCore.pyqtSlot()
    def actAbout_triggered(self):
        """" About program """
        myDlgAbout = dlgAbout.DlgAbout()
        myDlgAbout.exec_()

    def playSound(self):
        """ Play sound when new Emails received """
        path = os.getcwd() + os.path.sep + "Sounds" + os.path.sep
        if os.path.exists(path) and os.path.isdir(path):
            if os.name == "posix":
                if os.system("which alsa-utils") == 0:  # 256 if not installed
                    os.system("aplay {}beepMsg.wav&".format(path))
                elif os.system("which pulseaudio") == 0:
                    os.system("paplay {}beepMsg.wav&".format(path))
            if os.name == "nt":
                import winsound
                winsound.PlaySound("{}beepMsg.wav".format(path), winsound.SND_ASYNC)

    def closeEvent(self, e):
        """ Save settings when window is closing """

        self.setActionShowEnabled()
        # ignore closeEvent and fold program to systemTray
        e.ignore()
        self.writeSettings()
        self.hide()

    @QtCore.pyqtSlot()
    def actQuitApp_triggered(self):
        """ Quit program completely """
        self.writeSettings()
        self._settings.sync()
        QtWidgets.qApp.quit()


    @QtCore.pyqtSlot()
    def setActionHideEnabled(self):
        """" Set actHide Enabled and actShow Disabled in _trayMenu """
        self.ui.actHide.setEnabled(True)
        self.ui.actShow.setEnabled(False)

    @QtCore.pyqtSlot()
    def setActionShowEnabled(self):
        """" Set actShow Enabled and actHide Disabled in _trayMenu """
        self.ui.actHide.setEnabled(False)
        self.ui.actShow.setEnabled(True)

    @QtCore.pyqtSlot(QtWidgets.QSystemTrayIcon.ActivationReason)
    def sysTrayIcon_activated(self, reason):
        """ Mouse events handler """
#todo: process other reasons
        if self._sysTrIcon.isSystemTrayAvailable():
            if reason == 1:
                pass
                # print("Right click") Here is trayIcon context menu
            elif reason == 2:  # Double click
                pass
            elif reason == 3:  # Left click
        # todo: delete later
                # output result of checkEmails action
                # self._sysTrIcon.showMessage(self._translate("fMain", "Check Emailbox"),
                #                             self._infoAboutCheckedEmailsStr,
                #                             QtWidgets.QSystemTrayIcon.Information, 2000)

                # my own _popupMsg
                # not allow to invoke more than one _popupMsg
                if not self._popupMsg:
                    self._popupMsg = dlgPopupMsg.DlgPopupMsg(self)
                    self._popupMsg.closed.connect(self.dlgPopupMsg_closed)
                    self.dlgPopupMsgCalled.connect(self._popupMsg.dlgPopupMsgCalled_called)
                    # for login from popupMsg
                    self._popupMsg.loginTriggered.connect(self.login)
                    positionPopupMsg = self._positionPopupMsgDict.get(self.ui.cbxWhereShowPopup.currentIndex(),
                                                                      "Bottom right")
                    self.dlgPopupMsgCalled.emit(self._infoForDlgPopupMsgList, positionPopupMsg)
                    self._popupMsg.show()
            elif reason == 4:  # Middle click
                # show main window
                self.setActionHideEnabled()
                self.showNormal()
                self.activateWindow()
            elif reason == 0:  # Unknown click
                print("Unknown click")
        else:
            print("SystemTray is not available")

    @QtCore.pyqtSlot()
    def dlgPopupMsg_closed(self):
        """ Set self._popupMsg to False when dlgPopupMsg closed. """
        self._popupMsg = False

#     @QtCore.pyqtSlot()
#     def sysTrayIcon_msgClicked(self):
#         """ TrayIconMessage click event handler """
# #todo: process TrayIconMessage click
#         print("sysTrayIcon_msgClicked")

    def changeEvent(self, e):
        """
        Retranslate the app when called event LanguageChange.
        :param e:
        """
        if e.type() == QtCore.QEvent.LanguageChange:
            self.ui.retranslateUi(self)
            # to update translation in self._infoAboutCheckedEmailsStr ->
            # -> for systemTrayIcon Message and self.ui.lblStatusBarOutput
            if not self._isAnyCheckThreadRunning:
                self.actCheckEmbxs_triggered()
            else:
                self.ui.lblStatusBarOutput.setText(self._translate("fMain", "Checking. Please wait..."))
                self._sysTrIcon.setToolTip(self._translate("fMain", "Checking. Please wait..."))
        elif e.type() == QtCore.QEvent.WindowStateChange:
            if self.isMinimized():  # hide window to tray when window minimized
                self.setActionShowEnabled()
                self.writeSettings()
                self.hide()
        else:
            QtWidgets.QWidget.changeEvent(self, e)

    @QtCore.pyqtSlot()
    def actLangRus_triggered(self):
        """ Translate into Russian """
        self.ui.actLangEng.setChecked(False)
        self.ui.actLangRus.setChecked(True)

        self._fMainTranslator.load(":/translations/Translations/checkEmail_fMain_ru.qm")
        self._dlgAddEmbxTranslator.load(":/translations/Translations/checkEmail_dlgAddEmbx_ru.qm")
        self._dlgEdEmbxTranslator.load(":/translations/Translations/checkEmail_dlgEdEmbx_ru.qm")
        self._dlgAboutTranslator.load(":/translations/Translations/checkEmail_dlgAbout_ru.qm")
        self._dlgPopupMsgTranslator.load(":/translations/Translations/checkEmail_dlgPopupMsg_ru.qm")
        self._qtbasetranslator.load(":/translations/Translations/qtbase_ru.qm")

        QtWidgets.QApplication.installTranslator(self._dlgAboutTranslator)
        QtWidgets.QApplication.installTranslator(self._dlgAddEmbxTranslator)
        QtWidgets.QApplication.installTranslator(self._dlgEdEmbxTranslator)
        QtWidgets.QApplication.installTranslator(self._fMainTranslator)
        QtWidgets.QApplication.installTranslator(self._qtbasetranslator)

    @QtCore.pyqtSlot()
    def actLangEng_triggered(self):
        """ Translate into English """
        self.ui.actLangEng.setChecked(True)
        self.ui.actLangRus.setChecked(False)

        self._dlgPopupMsgTranslator.load(":/translations/Translations/checkEmail_dlgPopupMsg_en.qm")
        self._dlgAboutTranslator.load(":/translations/Translations/checkEmail_dlgAbout_en.qm")
        self._dlgEdEmbxTranslator.load(":/translations/Translations/checkEmail_dlgEdEmbx_en.qm")
        self._dlgAddEmbxTranslator.load(":/translations/Translations/checkEmail_dlgAddEmbx_en.qm")
        self._fMainTranslator.load(":/translations/Translations/checkEmail_fMain_en.qm")
        self._qtbasetranslator.load(":/translations/Translations/qtbase_en.qm")

        QtWidgets.QApplication.installTranslator(self._qtbasetranslator)

    def keyPressEvent(self, e):
        """ Hide app when Escape key pressed """
        if e.type() == QtCore.QEvent.KeyPress:
            if e.key() == QtCore.Qt.Key_Escape:
                self.setActionShowEnabled()
                self.writeSettings()
                self.hide()

    def eventFilter(self, obj, e):
        """ My eventFilter """
        if obj == self.ui.lwEmbxs:
            if e.type() == QtCore.QEvent.ContextMenu:
                self.createContextMenu()
                self._contextMenu.popup(e.globalPos())
                self._contextMenu.exec_()
                return True
            if e.type() == QtCore.QEvent.KeyPress:
                if e.key() == QtCore.Qt.Key_Delete:
                    self.actDelSelectedEmbxs_triggered()
                    return True
                elif e.key() == QtCore.Qt.Key_Up:
                    self.scrollUp()
                    return True
                elif e.key() == QtCore.Qt.Key_Down:
                    self.scrollDown()
                    return True
                elif e.key() == QtCore.Qt.Key_Right:  # cursor to the last row
                    self.ui.lwEmbxs.setCurrentRow(self.ui.lwEmbxs.count() - 1)
                    return True
                elif e.key() == QtCore.Qt.Key_Left:  # # cursor to the first row
                    self.ui.lwEmbxs.setCurrentRow(0)
                    return True
        return QtWidgets.QWidget.eventFilter(self, obj, e)

    def scrollUp(self):
        """ Scroll up one row in the lwEmbxs. """
        amountRows = self.ui.lwEmbxs.count()
        if self.ui.lwEmbxs.currentRow() != 0:  # move cursor up
            self.ui.lwEmbxs.setCurrentRow(self.ui.lwEmbxs.currentRow() - 1)
        else:  # first row
            self.ui.lwEmbxs.setCurrentRow(amountRows - 1)  # set cursor on the last row

    def scrollDown(self):
        """ Scroll down one row in the lwEmbxs. """
        amountRows = self.ui.lwEmbxs.count()
        # if self.ui.lwEmbxs.currentRow() != -1:  # selected
        if self.ui.lwEmbxs.currentRow() != amountRows - 1:  # move cursor down
            self.ui.lwEmbxs.setCurrentRow(self.ui.lwEmbxs.currentRow() + 1)
        else:  # last row
            self.ui.lwEmbxs.setCurrentRow(0)  # set cursor on the last row


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    # to prevent closing the app when the app is in tray and closed any dialog!!!!!!!!
    app.setQuitOnLastWindowClosed(False)
    window = MainWindow()
    # window.setWindowFlag(QtCore.Qt.Popup, True)
    # window.show()  for showing in tray - don't uncomment!!!!!!!
    sys.exit(app.exec_())
