#!/usr/bin/python3
# -*- coding: utf-8 -*-


from PyQt5 import QtWidgets, QtCore
from Gui import dlgEditEmailboxUI
from MyClasses import cEmailbox
import sqlite3


class DlgEditEmailbox(QtWidgets.QDialog):
    """ Dialog editing Emailbox """
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        self.ui = dlgEditEmailboxUI.Ui_dlgEditEmbx()
        self.ui.setupUi(self)
        # _oldFullEmail is to save old value of fullEmail for editing
        self._oldFullEmail = ""

        self._translate = QtCore.QCoreApplication.translate
        # block Adding while checking is running
        self._isAnyCheckThreadRunning = False

        self.conConnections()

    def conConnections(self):
        self.ui.btnCancel.clicked.connect(self.btnCancel_clicked)
        self.ui.btnEdEmbx.clicked.connect(self.btnEdEmbx_clicked)
        self.ui.chbxShowPass.stateChanged.connect(self.chbxShowPass_stateChanged)
        self.ui.ledFullEml.textChanged.connect(self.ledFullEml_textChanged)

    @QtCore.pyqtSlot(bool)
    def setIsAnyCheckThreadRunning(self, isRunning):
        """ Set isAnyCheckThreadRunning """
        self._isAnyCheckThreadRunning = isRunning

    @QtCore.pyqtSlot()
    def ledFullEml_textChanged(self):
        """ Trying to do smart choice of Email server """
        enteredServer = self.ui.ledFullEml.text().lower()
        smartChoice = {
            "@gmail": 0,
            "@yandex": 1,
            "@mail": 2,
            "@inbox": 2,
            "@list": 2,
            "@bk": 2,
            "@rambler": 3
        }
        for key in smartChoice:
            if key in enteredServer:
                self.ui.cbxEmlServer.setCurrentIndex(smartChoice[key])
                break
        else:
            self.ui.cbxEmlServer.setCurrentIndex(0)

    @QtCore.pyqtSlot()
    def btnCancel_clicked(self):
        self.close()

    @QtCore.pyqtSlot()
    def chbxShowPass_stateChanged(self):
        """ Show/Hide password """
        if self.ui.chbxShowPass.isChecked():
            self.ui.ledPass.setEchoMode(QtWidgets.QLineEdit.Normal)
        else:
            self.ui.ledPass.setEchoMode(QtWidgets.QLineEdit.Password)

    def __openDBandGetCEmailbox(self):
        """
        To avoid duplicate code:
        try to create instance of CEmailbox and get its.
        :return: CEmailbox instance
        """
        myCheckEmbx = cEmailbox.CEmailbox()
        try:
            myCheckEmbx.openDBandGetDict()
        except sqlite3.DatabaseError:
            QtWidgets.QMessageBox.critical(self,
            self._translate("dlgEditEmbx", "Opening database"),
            self._translate("dlgEditEmbx", "Database access error."))
        else:
            return myCheckEmbx

    @QtCore.pyqtSlot(str, str, str)
    def fMain_btnEdEmbxCalled(self, fullEmail, serverName, password):
        """
        My slot for fMain_btnEditEmailboxCalled signal.
        :param fullEmail
        :param serverName
        :param password
        """
        self.ui.ledFullEml.clear()
        # store oldFullEmail as keywork for sql deleting
        self.ui.ledFullEml.setText(fullEmail)
        self._oldFullEmail = fullEmail
        self.ui.ledPass.clear()
        self.ui.ledPass.setText(password)
        # restore serverName in cbxEmailServer for current fullEmail
        for index in range(self.ui.cbxEmlServer.count()):
            self.ui.cbxEmlServer.setCurrentIndex(index)
            if serverName == self.ui.cbxEmlServer.currentText():
                # self.ui.cbxEmlServer.setCurrentIndex(index)
                break

    @QtCore.pyqtSlot()
    def btnEdEmbx_clicked(self):
        """" Editing new Emailbox """

        # TODO: add regexp to check entering fullEmail

        fullEmail = self.ui.ledFullEml.text().strip()
        password = self.ui.ledPass.text().strip()
        if fullEmail == "":
            QtWidgets.QMessageBox.information(self,
            self._translate("dlgEditEmbx", "Edit Emailbox"),
            self._translate("dlgEditEmbx", "You must enter full Email address."))
            self.ui.ledFullEml.setFocus()
        elif password == "":
            QtWidgets.QMessageBox.information(self,
            self._translate("dlgEditEmbx", "Edit Emailbox"),
            self._translate("dlgEditEmbx", "You must enter password."))
            self.ui.ledPass.setFocus()
        else:
            # block access when another checking thread is running
            if self._isAnyCheckThreadRunning:
                QtWidgets.QMessageBox.warning(self,
                self._translate("dlgEditEmbx", "Check Emails"),
                self._translate("dlgEditEmbx", "Check Email task is running.\n"
                                "You can't edit an Emailbox\nuntill the task is running.\n"
                                "Wait untill the task will finish and try again."))
            else:
                myCheckEmbx = self.__openDBandGetCEmailbox()
                # allow fullEmails in the different case such as user@gmail.com and UsEr@GMail.Com.
                # get serverNamesDict and get serverName from key in cbxEmailServer
                dServerNames = myCheckEmbx.getServerNamesDict()
                serverKey = self.ui.cbxEmlServer.currentText()
                serverName = dServerNames[serverKey]
                try:
                    # deleting old record and insert new (changed) data into DB
                    myCheckEmbx.deleteOldThenInsertNewRecord(self._oldFullEmail, fullEmail, serverName, password)
                except sqlite3.DatabaseError:
                    QtWidgets.QMessageBox.critical(self,
                    self._translate("dlgEditEmbx", "Inserting data into DB"),
                    self._translate("dlgEditEmbx", "Error inserting Emailbox."))
                else:
                    QtWidgets.QMessageBox.information(self,
                    self._translate("dlgEditEmbx", "Inserting data into DB"),
                    self._translate("dlgEditEmbx", "Emailbox was successfully edited."))

                self.close()
