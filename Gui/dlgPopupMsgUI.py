# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dlgPopupMsgUI.ui'
#
# Created by: PyQt5 UI code generator 5.8
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_dlgPopupMsg(object):
    def setupUi(self, dlgPopupMsg):
        dlgPopupMsg.setObjectName("dlgPopupMsg")
        dlgPopupMsg.resize(568, 89)
        font = QtGui.QFont()
        font.setFamily("Comic Sans MS")
        font.setPointSize(12)
        dlgPopupMsg.setFont(font)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/icons/Icons/CheckEmail.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        dlgPopupMsg.setWindowIcon(icon)
        dlgPopupMsg.setWindowOpacity(1.0)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(dlgPopupMsg)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.lblLogo = QtWidgets.QLabel(dlgPopupMsg)
        self.lblLogo.setMaximumSize(QtCore.QSize(50, 46))
        font = QtGui.QFont()
        font.setPointSize(15)
        self.lblLogo.setFont(font)
        self.lblLogo.setText("")
        self.lblLogo.setPixmap(QtGui.QPixmap(":/icons/Icons/CheckEmail.ico"))
        self.lblLogo.setScaledContents(True)
        self.lblLogo.setAlignment(QtCore.Qt.AlignCenter)
        self.lblLogo.setObjectName("lblLogo")
        self.verticalLayout.addWidget(self.lblLogo)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.lwResults = QtWidgets.QListWidget(dlgPopupMsg)
        font = QtGui.QFont()
        font.setFamily("Tahoma")
        font.setPointSize(14)
        self.lwResults.setFont(font)
        self.lwResults.viewport().setProperty("cursor", QtGui.QCursor(QtCore.Qt.PointingHandCursor))
        self.lwResults.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.lwResults.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.lwResults.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.lwResults.setAlternatingRowColors(True)
        self.lwResults.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        self.lwResults.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectItems)
        self.lwResults.setResizeMode(QtWidgets.QListView.Fixed)
        self.lwResults.setObjectName("lwResults")
        self.horizontalLayout.addWidget(self.lwResults)
        self.verticalLayout_2.addLayout(self.horizontalLayout)

        self.retranslateUi(dlgPopupMsg)
        QtCore.QMetaObject.connectSlotsByName(dlgPopupMsg)

    def retranslateUi(self, dlgPopupMsg):
        _translate = QtCore.QCoreApplication.translate
        dlgPopupMsg.setWindowTitle(_translate("dlgPopupMsg", "Check Email"))

import resources_rc
