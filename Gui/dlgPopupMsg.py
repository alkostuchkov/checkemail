# -*- coding: utf-8 -*-

from PyQt5.QtWidgets import QDialog, QDesktopWidget, QApplication
from PyQt5.QtCore import pyqtSlot, pyqtSignal, QTimer, QThreadPool, QRect
from Gui import dlgPopupMsgUI
from ThreadsClasses import cPopupMsg


class DlgPopupMsg(QDialog):
    closed = pyqtSignal()
    mouseEntered = pyqtSignal()
    loginTriggered = pyqtSignal(str)

    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.ui = dlgPopupMsgUI.Ui_dlgPopupMsg()
        self.ui.setupUi(self)

        self.tmrShowPopupMsg = QTimer(self)
        self.tmrShowPopupMsg.setSingleShot(True)
        self.tmrShowPopupMsgInterval = 2000
        self.tmrShowPopupMsg.start(self.tmrShowPopupMsgInterval)

        self.threadPool = QThreadPool()
        # self.threadPopupMsg = False

        self.tmrShowPopupMsg.timeout.connect(self.createThread)
        self.ui.lwResults.itemClicked.connect(self.lwItemClicked_clicked)

    def calculatePopupMsgSize(self):
        """ Calculate dlgPopupMsg width and height depends on amount and width of results. """
        maxRecordLength = 25
        for i in range(self.ui.lwResults.count()):
            self.ui.lwResults.setCurrentRow(i)
            if len(self.ui.lwResults.currentItem().text()) > maxRecordLength:
                maxRecordLength = len(self.ui.lwResults.currentItem().text())

        # limit for dlg width not to be wider than screen's width
        width = maxRecordLength * 12 + 30
        if width > QApplication.desktop().width():
            width = QApplication.desktop().width()

        amountRecords = self.ui.lwResults.count()
        # limit for dlg height not to be higher than screen's height
        height = amountRecords * 20 + self.height()
        if height > QApplication.desktop().height():
            height = QApplication.desktop().height()

        self.ui.lwResults.setCurrentRow(-1)
        return (width, height)

    def calculatePopupMsgPosition(self, widthPopupMsg, heightPopupMsg, position):
        """ Calculate dlgPopupMsg position depends on user choice in cbxWhereShowPopup. """
        # Screen's width and height without taskbar
        screenWidth = QDesktopWidget().availableGeometry().width()
        screenHeight = QDesktopWidget().availableGeometry().height()
        margin = 5  # margin

        # calculate x and y coordinates for popupMsg
        if position == "Bottom right":
            x = screenWidth - (widthPopupMsg + margin)
            y = screenHeight - (heightPopupMsg + margin)
        elif position == "Bottom left":
            x = margin
            y = screenHeight - (heightPopupMsg + margin)
        elif position == "Top right":
            x = screenWidth - (widthPopupMsg + margin)
            y = margin * 7
        elif position == "Top left":
            x = margin * 2
            y = margin * 7

        self.setGeometry(x, y, widthPopupMsg, heightPopupMsg)

        # print(QDesktopWidget.height(self))
        # print(QDesktopWidget.width(self))
        # print(QApplication.desktop().height())
        # print(QApplication.desktop().width())
        #
        # print(QDesktopWidget().screenGeometry().height())
        # print(QDesktopWidget().screenGeometry().width())
        # print(QDesktopWidget().availableGeometry().height())
        # print(QDesktopWidget().availableGeometry().width())

    @pyqtSlot()
    def createThread(self):
#todo delete
        print("timeout")
        threadPopupMsg = cPopupMsg.CPopupMsgRunnable()
        threadPopupMsg.signals.result.connect(self.opacityValueChanged_changed)
        threadPopupMsg.signals.finished.connect(self.close)
        self.closed.connect(threadPopupMsg.signals.cancel)
        self.mouseEntered.connect(threadPopupMsg.cancel)
        self.threadPool.start(threadPopupMsg)

    @pyqtSlot(list, str)
    def dlgPopupMsgCalled_called(self, checkingResultList, positionStr):
        """ Slot for parse result list for _popupMsg. """
        self.ui.lwResults.clear()
        for result in checkingResultList:
            self.ui.lwResults.addItem(result)

        # calculate dlgPopupMsg width and height
        widthPopupMsg, heightPopupMsg = self.calculatePopupMsgSize()
        # calculate dlgPopupMsg position
        self.calculatePopupMsgPosition(widthPopupMsg, heightPopupMsg, positionStr)

    @pyqtSlot(int)
    def opacityValueChanged_changed(self, opacity):
        if opacity != 0:
            self.setWindowOpacity(opacity / 100)

    def enterEvent(self, e):
        """ When mouse entered the dialog. """
        self.setWindowOpacity(1.0)
        self.tmrShowPopupMsg.stop()
        self.mouseEntered.emit()
        QDialog.enterEvent(self, e)

    def leaveEvent(self, e):
        """ When mouse left the dialog. """
        self.tmrShowPopupMsg.start(self.tmrShowPopupMsgInterval)
        QDialog.leaveEvent(self, e)

    @pyqtSlot()
    def lwItemClicked_clicked(self):
        """ Get fullEmail and call authorization when item in popupMsg clicked. """
        rowText = self.ui.lwResults.currentItem().text()
        fullEmail = rowText.split()[0]
        self.loginTriggered.emit(fullEmail)
        self.close()

    def closeEvent(self, e):
        """ Let thread to finish. """
        # self.hide()
        self.tmrShowPopupMsg.stop()
        self.closed.emit()
        self.threadPool.waitForDone(3000)
        e.accept()
