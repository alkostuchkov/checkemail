#!/usr/bin/python3
# -*- coding: utf-8 -*-

from PyQt5 import QtWidgets, QtCore
from Gui import dlgAddEmailboxUI
from MyClasses import cEmailbox
import sqlite3


class DlgAddEmail(QtWidgets.QDialog):
    """ Dialog a add new Email box """
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        self.ui = dlgAddEmailboxUI.Ui_dlgAddEmbx()
        self.ui.setupUi(self)

        self._translate = QtCore.QCoreApplication.translate
        # block Adding while checking is running
        self._isAnyCheckThreadRunning = False

        self.conConnections()

    def conConnections(self):
        self.ui.btnCancel.clicked.connect(self.btnCancel_clicked)
        self.ui.btnAddEmbx.clicked.connect(self.btnAddEmbx_clicked)
        self.ui.chbxShowPass.stateChanged.connect(self.chbxShowPass_stateChanged)
        self.ui.ledFullEml.textChanged.connect(self.ledFullEml_textChanged)
        
    @QtCore.pyqtSlot(bool)
    def setIsAnyCheckThreadRunning(self, isRunning):
        """ Set isAnyCheckThreadRunning """
        self._isAnyCheckThreadRunning = isRunning

    @QtCore.pyqtSlot()
    def ledFullEml_textChanged(self):
        """ Trying to do smart choice of Email server """
        enteredServer = self.ui.ledFullEml.text().lower()
        smartChoice = {
            "@gmail": 0,
            "@yandex": 1,
            "@mail": 2,
            "@inbox": 2,
            "@list": 2,
            "@bk": 2,
            "@rambler": 3
            }
        for key in smartChoice:
            if key in enteredServer:
                self.ui.cbxEmlServer.setCurrentIndex(smartChoice[key])
                break
        else:
            self.ui.cbxEmlServer.setCurrentIndex(0)

    @QtCore.pyqtSlot()
    def btnCancel_clicked(self):
        self.close()

    @QtCore.pyqtSlot()
    def chbxShowPass_stateChanged(self):
        """ Show/Hide password """
        if self.ui.chbxShowPass.isChecked():
            self.ui.ledPass.setEchoMode(QtWidgets.QLineEdit.Normal)
        else:
            self.ui.ledPass.setEchoMode(QtWidgets.QLineEdit.Password)

    @QtCore.pyqtSlot()
    def btnAddEmbx_clicked(self):
        """" Adding new Emailbox """

        #TODO: add regexp to check entering fullEmail

        fullEmail = self.ui.ledFullEml.text().strip()
        password = self.ui.ledPass.text().strip()
        if fullEmail == "":
            QtWidgets.QMessageBox.information(self,
            self._translate("dlgAddEmbx", "Add Emailbox"),
            self._translate("dlgAddEmbx", "You must enter full Email address."))
            self.ui.ledFullEml.setFocus()
        elif password == "":
            QtWidgets.QMessageBox.information(self,
            self._translate("dlgAddEmbx", "Add Emailbox"),
            self._translate("dlgAddEmbx", "You must enter password."))
            self.ui.ledPass.setFocus()
        else:  # block access when another checking thread is running
            if self._isAnyCheckThreadRunning:
                QtWidgets.QMessageBox.warning(self,
                self._translate("dlgAddEmbx", "Check Emails"),
                self._translate("dlgAddEmbx", "Check Email task is running.\n"
                                "You can't add an Emailbox\nuntill the task is running.\n"
                                "Wait untill the task will finish and try again."))
            else:  # check if the adding Email exists in the DB.
                myCheckEmbx = cEmailbox.CEmailbox()
                try:
                    myCheckEmbx.openDBandGetDict()
                except sqlite3.DatabaseError:
                    QtWidgets.QMessageBox.critical(self,
                    self._translate("dlgAddEmbx", "Opening database"),
                    self._translate("dlgAddEmbx", "Database access error."))
                else:  # to avoid duplicates of fullEmails in the different case such as user and User.
                    dCheckEmailbox = myCheckEmbx.getCheckEmailboxDict()
                    isFullEmailboxExists = False
                    for key in dCheckEmailbox.keys():
                        if fullEmail.lower() in str(key).lower():
                            isFullEmailboxExists = True
                            break
                    if isFullEmailboxExists:
                        QtWidgets.QMessageBox.warning(self,
                        self._translate("dlgAddEmbx", "Add Emailbox"),
                        self._translate("dlgAddEmbx", "Emailbox {} is already exists.\n"
                                        "For editing click 'Cancel'\nand choose in the main window "
                                        "'Edit'.".format(fullEmail)))
                    else:  # get serverNamesDict and get serverName from key in cbxEmailServer
                        dServerNames = myCheckEmbx.getServerNamesDict()
                        serverKey = self.ui.cbxEmlServer.currentText()
                        serverName = dServerNames[serverKey]
                        try:  # insert data into DB
                            myCheckEmbx.insertIntoDB(fullEmail, serverName, password)
                        except sqlite3.DatabaseError:
                            QtWidgets.QMessageBox.critical(self,
                            self._translate("dlgAddEmbx", "Inserting data into DB"),
                            self._translate("dlgAddEmbx", "Error inserting Emailbox."))
                        else:
                            QtWidgets.QMessageBox.information(self,
                            self._translate("dlgAddEmbx", "Inserting data into DB"),
                            self._translate("dlgAddEmbx", "Email was successfully added."))

                        self.close()
