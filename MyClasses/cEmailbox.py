#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import sqlite3
import imaplib
from MyExceptions.myExceptions import MyIMAP4ConnectionError, MyIMAP4LoginError


class CEmailbox:
    """ My class for working with DB and emails """
    # # SIGNAL if a new Email received
    # newEmailReceived = QtCore.pyqtSignal(bool)

    def __init__(self):
        self._checkEmailboxDict = {}
        self._pathToDB = os.getcwd() + os.path.sep + "Database" + os.path.sep
        # self._pathToDB = "Database"
        self._serverNamesDict = {
            "google": "imap.gmail.com",
            "yandex": "imap.yandex.com",
            "mailru": "imap.mail.ru",
            "inbox": "imap.mail.ru",
            "list": "imap.mail.ru",
            "bk": "imap.mail.ru",
            "rambler": "imap.rambler.ru"
        }
        self._isDbEmpty = True
        self._isNewEmails = False

    def getCheckEmailboxDict(self):
        return self._checkEmailboxDict

    def setCheckEmailboxDict(self, checkEmailboxDict):
        self._checkEmailboxDict = checkEmailboxDict

    def getServerNamesDict(self):
        return self._serverNamesDict

    def setServerNamesDict(self, serverNamesDict):
        self._serverNamesDict = serverNamesDict

    def getIsNewEmails(self):
        return self._isNewEmails

    def getIsDbEmpty(self):
        return self._isDbEmpty

    def openDBandGetDict(self):
        """
        Create database and tables if they not exist.
        Get fullEmail, serverName, password from DB
        """
        # if dir 'Database' not exists or not a dir, create this.
        if not os.path.exists(self._pathToDB) or not os.path.isdir(self._pathToDB):
            os.makedirs(self._pathToDB)

        conn = sqlite3.connect(self._pathToDB + "CheckEmailbox.db")
        conn.execute("PRAGMA foreign_keys=1")  # enable cascade deleting and updating.
        cur = conn.cursor()
        sql = """\
                CREATE TABLE IF NOT EXISTS emailboxes(
                   id INTEGER PRIMARY KEY NOT NULL,
                   fullEmail TEXT UNIQUE NOT NULL COLLATE BINARY,
                   serverName TEXT NOT NULL COLLATE NOCASE,
                   password TEXT NOT NULL
                );
                """
        try:
            cur.executescript(sql)
        except sqlite3.DatabaseError:
            raise sqlite3.DatabaseError  # ("Не удалось создать DB.")
        else:
            sql = """\
            SELECT emailboxes.fullEmail, emailboxes.serverName, emailboxes.password
            FROM emailboxes;
            """
            try:
                cur.execute(sql)
            except sqlite3.DatabaseError:
                raise sqlite3.DatabaseError  # ("Не удалось выполнить запрос.")
            else:
                for fullEmail, serverName, password in cur:
                    # decode data getting from DB
                    fullEmail = self.encodeDecodeData(fullEmail)
                    serverName = self.encodeDecodeData(serverName)
                    password = self.encodeDecodeData(password)

                    self._checkEmailboxDict.setdefault(fullEmail, [])
                    self._checkEmailboxDict[fullEmail].append(serverName)
                    self._checkEmailboxDict[fullEmail].append(password)
        finally:
            cur.close()
            conn.close()
            # set self._isDbEmpty
            if len(self._checkEmailboxDict) != 0:
                self._isDbEmpty = False
            else:
                self._isDbEmpty = True

    def encodeDecodeData(self, data):
        """ Encode/Decode input data """
        outputResult = ""
        for char in data:
            outputResult += chr(ord(char) ^ 7677)
        return outputResult

    def insertIntoDB(self, fullEmail, serverName, password):
        """ Insert data to the database. """
        # encode params
        fullEmail = self.encodeDecodeData(fullEmail)
        serverName = self.encodeDecodeData(serverName)
        password = self.encodeDecodeData(password)

        conn = sqlite3.connect(self._pathToDB + "CheckEmailbox.db")
        conn.execute("PRAGMA foreign_keys=1")  # enable cascade deleting and updating.
        cur = conn.cursor()
        try:
            cur.execute("INSERT INTO emailboxes(fullEmail, serverName, password) "
                        "VALUES(:fullEmail, :serverName, :password)",
                        {"fullEmail": fullEmail, "serverName": serverName, "password": password})
        except sqlite3.DatabaseError as err:
            raise sqlite3.DatabaseError("insertIntoDb: INSERT INTO emailboxes", err)
        else:
            conn.commit()  # complete transactions.
        finally:
            cur.close()
            conn.close()

    def multiDeletingRecords(self, fullEmailsList):
        """
        Multi Deleting Emailboxes from the database.
        :param fullEmailsList:
        """
        conn = sqlite3.connect(self._pathToDB + "CheckEmailbox.db")
        conn.execute("PRAGMA foreign_keys=1")  # enable cascade deleting and updating.
        cur = conn.cursor()
        try:
            for fullEmail in fullEmailsList:
                # encode fullEmail because all data in the DB are stored encoded.
                fullEmail = self.encodeDecodeData(fullEmail)
                try:  # cascade deleting all records from names and phoneNumbers for THIS name.
                    cur.execute("DELETE FROM emailboxes WHERE fullEmail=:fullEmail", {"fullEmail": fullEmail})
                except sqlite3.DatabaseError as err:
                    raise sqlite3.DatabaseError("multiDeletingRecords", err)
            conn.commit()  # commit transactions after completion all deleting.
        finally:
            cur.close()
            conn.close()

    def clearDB(self):
        """ Delete all data from the database. """
        conn = sqlite3.connect(self._pathToDB + "CheckEmailbox.db")
        conn.execute("PRAGMA foreign_keys=1")  # enable cascade deleting and updating.
        cur = conn.cursor()
        sql = """\
                DELETE FROM emailboxes;
                """
        try:
            cur.executescript(sql)
        except sqlite3.DatabaseError:
            raise sqlite3.DatabaseError  # ("Не удалось выполнить запрос.")
        else:
            conn.commit()  # complete transaction.
        finally:
            cur.close()
            conn.close()

    def deleteOldThenInsertNewRecord(self, oldFullEmail, newFullEmail, newServerName, newPassword):
        """
        Delete and then insert data.
        :param oldFullEmail
        :param newFullEmail
        :param newServerName
        :param newPassword
        """
        conn = sqlite3.connect(self._pathToDB + "CheckEmailbox.db")
        conn.execute("PRAGMA foreign_keys=1")  # enable cascade deleting and updating.
        cur = conn.cursor()
        # encode all data because all data in the DB are stored encoded.
        oldFullEmail = self.encodeDecodeData(oldFullEmail)
        newFullEmail = self.encodeDecodeData(newFullEmail)
        newServerName = self.encodeDecodeData(newServerName)
        newPassword = self.encodeDecodeData(newPassword)
        try:
            # cascade deleting all records from emailboxes
            cur.execute("DELETE FROM emailboxes WHERE fullEmail=:oldFullEmail", {"oldFullEmail": oldFullEmail})
            # inserting newFullEmail, serverName, password
            # self.insertIntoDB(newFullEmail, newServerName, newPassword)
            # TODO: Delete if above string works!!!!!!!!
            cur.execute("INSERT INTO emailboxes(fullEmail, serverName, password) "
                        "VALUES(:newFullEmail, :newServerName, :newPassword)",
                        {"newFullEmail": newFullEmail, "newServerName": newServerName, "newPassword": newPassword})
        except sqlite3.DatabaseError as err:
            raise sqlite3.DatabaseError("DeleteOldThenInsertNewRecord", err)
        else:  # only if all inserting completed successfully, commit!!!
            conn.commit()  # complete transactions
        finally:
            cur.close()
            conn.close()

    def checkEmails(self, selectedEmbxsList=[]):
        """
        Check new emails in all Emailboxes.
        :param selectedEmbxsList (if selectedEmbxsList is empty - [] check ALL Emailboxes)
        :returns resultDict for ALL Emailboxes like
        {"fullEmail": [totalEmails, totalUnseenEmails]} if NoErrors OR
        {"fullEmail": [totalEmails, totalUnseenEmails, "ConnectionError" or "LoginError"]} if Errors
        """
        self._isNewEmails = False
        resultDict = {}
        if len(selectedEmbxsList) == 0:  # check all Emailboxes
            selectedEmbxsList = self._checkEmailboxDict
        for fullEmail in sorted(self._checkEmailboxDict):
            if fullEmail in selectedEmbxsList:
                serverName = self._checkEmailboxDict[fullEmail][0]
                password = self._checkEmailboxDict[fullEmail][1]
                # update resultDict by returned dict from __checkEmailsExtra
        #todo: TypeError 'NoneType'
                # try:  here can be try because if no internet there will be Exception
                resultDict.update(self.__checkEmailsExtra(fullEmail, serverName, password))
                # except MyIMAP4ConnectionError:
                #     raise MyIMAP4ConnectionError
                # except MyIMAP4LoginError:
                #     print("MyIMAP4ConnectionError in cEmailbox.py")
                #     raise MyIMAP4LoginError

        return resultDict

    def __checkEmailsExtra(self, fullEmail, serverName, password):
        """
        Called from checkEmails.
        Check new emails in passed Emailbox.
        If error occurs self.__isCheckError will set True
        :param fullEmail -> userName@serverName.domen
        :param serverName -> imap.gmail.com
        :param password
        :returns resultDict for passed fullEmail like {"fullEmail": [totalEmails, totalUnseenEmails]}
        """
        port = 993
        resultDict = {fullEmail: ["??", "??"]}
        try:
            imapServer = imaplib.IMAP4_SSL(serverName, port)
        except Exception:
            # if LoginError don't raise exception
            # just append record like {fullEmail: ["??", "??", "ConnectionError]}
            resultDict[fullEmail].append("ConnectionError")
            # raise MyIMAP4ConnectionError
        else:
            try:
                imapServer.login(fullEmail, password)
            except Exception:
                # if LoginError don't raise exception
                # just append record like {fullEmail: ["??", "??", "LoginError]}
                resultDict[fullEmail].append("LoginError")
                # raise MyIMAP4LoginError
            else:
                # check INBOX readonly=True
                typ, data = imapServer.select("INBOX", True)
                if typ == "OK":
                    # get totalEmails
                    totalEmails = int(data[0])
                    # search all new UNSEEN emails
                    typ, data = imapServer.search(None, "UNSEEN")
                    if typ == "OK":
                        # split bytes in data to list and get its len
                        totalUnseenEmails = len(data[0].split())
                        # set flag if at least one new Email's received
                        if totalUnseenEmails != 0 and not self._isNewEmails:
                            self._isNewEmails = True
                        # correct data in resultDict
                        resultDict[fullEmail][0] = totalEmails
                        resultDict[fullEmail][1] = totalUnseenEmails

                imapServer.logout()
            finally:
                return resultDict
        finally:
            return resultDict


