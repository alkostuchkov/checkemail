#!/usr/bin/python3
# -*- coding: utf-8 -*-

from selenium import webdriver
from selenium.common import exceptions
from selenium.webdriver.opera import options
import time
import os
import sys
import json
import shutil
import subprocess
import platform
from MyExceptions import myExceptions

# from selenium.webdriver.common.by import By
# from selenium.webdriver.support.ui import WebDriverWait
# from selenium.webdriver.support import expected_conditions as EC
# import traceback


class CAuthentification:
    """ Class for authentification on Email servers. """

    def __init__(self):

        self._pathToAppBinary = os.path.abspath(sys.argv[0])
        self._pathToAppDir = os.path.dirname(self._pathToAppBinary) + os.path.sep
        self._pathToWebdrivers = self._pathToAppDir + "Webdrivers" + os.path.sep

        self._firefoxNames = ("firefox", "iceweasel", "iceape", "firefoxurl"
                              "seamonkey", "mozilla-firefox", "mozilla-firebird",
                              "firebird", "mozilla", "netscape"
                              )
        self._chromeNames = ("google-chrome", "chrome", "chromium", "chromium-browser", "chromehtml")
        self._operaNames = ("opera", "opera-browser")

        self._defaultBrowserName = self._getDefaultBrowserName()
        self._pathToBrowserBinary = self._getPathToBrowserBinary(self._defaultBrowserName)
        self._driver = None

    def _getDefaultBrowserName(self):
        """ Get default browser name. """
        if sys.platform[:3] == "win":
            import winreg

            hkcu = winreg.HKEY_CURRENT_USER
            path = r"SOFTWARE\Microsoft\Windows\Shell\Associations\UrlAssociations\http\UserChoice"
            try:
                with winreg.OpenKey(hkcu, path) as key:
                    cmd = winreg.QueryValueEx(key, "ProgId")
            except FileNotFoundError:
                # "Can't find the way HKCU\{}. Maybe this is Windows XP.".format(path))
                return "?"  #"Internet Explorer"
            else:
                for browserName in self._firefoxNames:
                    if cmd[0].lower().startswith(browserName):
                        return "Mozilla Firefox"
                for browserName in self._operaNames:
                    if cmd[0].lower().startswith(browserName):
                        return "Opera"
                for browserName in self._chromeNames:
                    if cmd[0].lower().startswith(browserName):
                        return "Google"
                if "ie" in cmd[0].lower():
                    return "Internet Explorer"
                else:  # if unknown default, it will be Internet Explorer.
                    return "Internet Explorer"
        else:  # [:-8] deletes .desktop
            defaultBrowser = subprocess.check_output(["xdg-settings", "get", "default-web-browser"]).rstrip().decode("utf-8")[:-8]
            if defaultBrowser in self._firefoxNames:
                return "Mozilla Firefox"
            elif defaultBrowser in self._operaNames:
                return "Opera"
            elif defaultBrowser in self._chromeNames:
                return "Google"
            else:  # if unknown default, it'll be Firefox
                return "Mozilla Firefox"

    def _getPathToBrowserBinary(self, browserName):
        """ Get path to browser binary. """
        pathToBrowserBinary = ""
        # if os.name == "nt":
        if sys.platform[:3] == "win":
            pathToAppData = os.environ["APPDATA"]
            pathToBrowser = os.environ.get("PROGRAMW6432",
                                           os.environ["PROGRAMFILES"]) + os.path.sep + browserName + os.path.sep
            if browserName == "Opera":
                pathToJsonFile = pathToBrowser + "installation_status.json"
                with open(pathToJsonFile, "r") as f:
                    optJson = f.read().rstrip()

                optDict = json.loads(optJson)
                pathToBrowserBinary = pathToBrowser + optDict["version"] + os.path.sep + "opera.exe"
            elif browserName == "Mozilla Firefox":
                pathToBrowserBinary = pathToBrowser + "firefox.exe"
            elif browserName == "Google":
                pathToBrowserBinary = pathToBrowser + "Chrome" + os.path.sep + "Application" + os.path.sep + "chrome.exe"
            elif browserName == "Internet Explorer":
                pathToBrowserBinary = pathToBrowser + "iexplore.exe"
            elif browserName == "Edge":  #todo: Name???
                # _pathToBrowserBinary = pathToBrowser + "edge.exe"
                pass
            elif browserName == "?":  # Windows XP
                pathToBrowserBinary = "?"
        else:
            if browserName == "Mozilla Firefox":
                for browser in self._firefoxNames:
                    pathToBrowserBinary = shutil.which(browser)
                    break
            elif browserName == "Google":
                for browser in self._firefoxNames:
                    pathToBrowserBinary = shutil.which(browser)
                    break
            elif browserName == "Opera":
                for browser in self._operaNames:
                    pathToBrowserBinary = shutil.which(browser)
                    break
            else:  # if unknown browserName Mozilla Firefox will be its.
                pathToBrowserBinary = os.environ["HOME"] + "/Programs/FirefoxBeta/firefox"

        return pathToBrowserBinary

    def _getDriver(self, browserName, pathToBrowserBinary):  #todo: add browserProfile if need
        """ Get webdriver for current browser. """
        # # no logging webdriver
        # if sys.platform[:3] == "win":
        #     serviceLogPath = "NUL"
        # else:
        #     serviceLogPath = "/dev/null"

        if browserName == "Mozilla Firefox":
            if sys.platform[:3] == "win":
                if platform.machine().endswith("64"):
                    driverName = "geckodriver_win64.exe"
                else:
                    driverName = "geckodriver_win32.exe"
            else:
                if platform.machine().endswith("64"):
                    driverName = "geckodriver_lin64"
                else:
                    driverName = "geckodriver_lin32"

            firefoxOptions = webdriver.FirefoxOptions()
            # firefoxOptions.add_argument("--silent")
            # firefoxOptions.headless
            # firefoxOptions.add_argument("-headless")  # don't show geckodriver's console
            try:
                driver = webdriver.Firefox(firefox_binary=pathToBrowserBinary,
                                           options=firefoxOptions,
                                           service_log_path=os.devnull,
                                           executable_path=self._pathToWebdrivers + driverName)
            except exceptions.WebDriverException:
                raise FileNotFoundError("FileNotFound:\n{}".format(self._pathToWebdrivers + driverName))
        #todo: FULL
        # _driver = webdriver.Firefox(firefox_binary=pathToFirefoxBinary,
        #                            executable_path=(_pathToAppDir + "geckodriver.exe"),
        #                            firefox_profile=r"C:\Users\Alex\AppData\Roaming\Mozilla\Firefox\Profiles\8p36nrio.default-release",
        #                            desired_capabilities=webdriver.DesiredCapabilities.FIREFOX)
        elif browserName == "Opera":
            if sys.platform[:3] == "win":
                if platform.machine().endswith("64"):
                    driverName = "operadriver_win64.exe"
                else:
                    driverName = "operadriver_win32.exe"
            else:
                if platform.machine().endswith("64"):
                    driverName = "operadriver_lin64"
                else:
                    pass
                    # print("no operadriver_lin32.exe")

            operaOptions = webdriver.opera.options.ChromeOptions()
            # operaOptions.headless
            # operaOptions.add_argument("-headless")  # don't show operadriver's console
            # operaOptions.add_argument("user-data-dir=" + pathToAppData + os.path.sep + "Opera Software\Opera Stable")
            operaOptions.binary_location = pathToBrowserBinary
            try:
                driver = webdriver.Opera(executable_path=self._pathToWebdrivers + driverName,
                                         options=operaOptions,
                                         service_log_path=os.devnull,
                                         desired_capabilities=webdriver.DesiredCapabilities.OPERA)
            except exceptions.WebDriverException:
                raise FileNotFoundError("FileNotFound:\n{}".format(self._pathToWebdrivers + driverName))
        elif browserName == "Google":
            if sys.platform[:3] == "win":
                if platform.machine().endswith("64"):
                    driverName = "chromedriver_win32.exe"
                else:
                    driverName = "chromedriver_win32.exe"
            else:
                if platform.machine().endswith("64"):
                    driverName = "chromedriver_lin64"
                else:
                    pass
                    # print("no chromedriver_lin32")
            try:
                driver = webdriver.Chrome(executable_path=self._pathToWebdrivers + driverName,
                                                                    service_log_path=os.devnull)
            except exceptions.WebDriverException:
                raise FileNotFoundError("FileNotFound:\n{}".format(self._pathToWebdrivers + driverName))
        elif browserName == "Internet Explorer":
            # if sys.platform[:3] == "win":
            #     if platform.machine().endswith("64"):
            #         driverName = "IEDriverServer_win64.exe"
            #     else:
            #         driverName = "IEDriverServer_win32.exe"
            # ieOptions = webdriver.IeOptions()
            # try:
            #     driver = webdriver.Ie(executable_path=self._pathToWebdrivers + driverName)
            # except exceptions.WebDriverException:
            #     raise FileNotFoundError("FileNotFound: {}".format(self._pathToWebdrivers + driverName))
            driver = None
        elif browserName == "Edge":
            # if sys.platform[:3] == "win":
            #     if platform.machine().endswith("64"):
            #         driverName = "msedgedriver_win64.exe"
            #     else:
            #         driverName = "msedgedriver_win32.exe"
            # driver = webdriver.Edge(executable_path=self._pathToWebdrivers + driverName)
            # driver = webdriver.Edge()
            driver = None
        elif browserName == "?" and pathToBrowserBinary == "?":  # Windows XP
            # "Maybe you still use Windows XP?"
            driver = None

        self._driver = driver

    def authGMail(self, login, password):
        """ GMail authentification. """
        # if self._driver in not None self._driver has been already got!
        self._getDriver(self._defaultBrowserName, self._pathToBrowserBinary)

        if self._driver is not None:
            self._driver.maximize_window()
            try:
                self._driver.get("https://gmail.com")
                # self._driver.get("https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin")
            except exceptions.WebDriverException:
                # No internet
                return
            try:
                try:
                    self._driver.find_element_by_id("identifierId").send_keys(login)
                    self._driver.find_element_by_id("identifierNext").click()
                # except exceptions.NoSuchElementException:
                except Exception:
                    raise myExceptions.MyEmailboxLoginError("Cannot login.")
                else:
                    time.sleep(5)
                    try:
                        self._driver.find_element_by_name("password").send_keys(password)
                        self._driver.find_element_by_id("passwordNext").click()
                    # except exceptions.NoSuchElementException:
                    except Exception:
                        raise myExceptions.MyEmailboxLoginError("Cannot login")
                finally:
                    pass
                    # self._driver.close()
                    # self._driver.quit()
            # except exceptions.WebDriverException:
            except Exception:
                # No internet?")
                raise myExceptions.MyEmailboxLoginError("Cannot login")
        else:
            raise myExceptions.MyBadBrowserOrOSError("BadBrowserOrOS")

    def authMailru(self, login, password):
        """ Mailru authentification. """
        self._getDriver(self._defaultBrowserName, self._pathToBrowserBinary)

        if self._driver is not None:
            self._driver.maximize_window()
            try:
                self._driver.get("https://mail.ru")
            except exceptions.WebDriverException:
                # No internet
                return
            time.sleep(1)
            try:
                loginField = self._driver.find_element_by_id("mailbox:login")
                loginField.send_keys(login)
                time.sleep(1)
                loginField.submit()

                time.sleep(1)
                passField = self._driver.find_element_by_name("password")
                passField.send_keys(password)
                time.sleep(1)
                passField.submit()

                # self._driver.find_element_by_id("mailbox:login").send_keys(login)
                # passField = self._driver.find_element_by_id("mailbox:password")
                # time.sleep(1)
                # passField.send_keys(password)
                # time.sleep(1)
                # passField.submit()
            except Exception:
                raise myExceptions.MyEmailboxLoginError("Cannot login")
        else:
            raise myExceptions.MyBadBrowserOrOSError("BadBrowserOrOS")

    def authYandex(self, login, password):
        """ Yandex authentification. """
        self._getDriver(self._defaultBrowserName, self._pathToBrowserBinary)

        if self._driver is not None:
            self._driver.maximize_window()
            try:
                self._driver.get("https://passport.yandex.com")
            except exceptions.WebDriverException:
                # No internet
                return
            try:
                try:
                    self._driver.find_element_by_id("passp-field-login").send_keys(login)
                    self._driver.find_element_by_class_name("passp-sign-in-button").click()
                    # self._driver.find_element_by_css_selector("div.passp-button:nth-child(12)").click()
                except Exception:
                    raise myExceptions.MyEmailboxLoginError("Cannot login.")
                else:
                    time.sleep(5)
                    try:
                        self._driver.find_element_by_id("passp-field-passwd").send_keys(password)
                        self._driver.find_element_by_class_name("passp-sign-in-button").click()
                    except Exception:
                        raise myExceptions.MyEmailboxLoginError("Cannot login")
                finally:
                    pass
                    # self._driver.close()
                    # self._driver.quit()
            except Exception:
                # No internet?")
                raise myExceptions.MyEmailboxLoginError("Cannot login")
        else:
            raise myExceptions.MyBadBrowserOrOSError("BadBrowserOrOS")

    def authRambler(self, login, password):
        """ Rambler authentification. """
        pass


if __name__ == "__main__":
    auth = CAuthentification()
    auth.authGMail("user@mail.com", "password")

# try:
#     elementPassword = WebDriverWait(_driver, 10).until(
#         EC.presence_of_element_located((By.NAME, "password"))
#     )
#     elementPassword.send_keys(password)
# except:
#     print("element password not found.")
# else:
#     _driver.find_element_by_id("passwordNext").click()
#     # _driver.quit()

